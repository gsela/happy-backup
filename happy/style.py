## @package happy.style
#  @brief Collection of methods to produce plots following ATLAS style guidelines.
#
#  @author Christian Grefe, Bonn University (christian.grefe@cern.ch)
from happy.miscellaneous import loadRootMacro
from array import array
import os, copy

## Load the ATLAS style ROOT C macro.
#  @param path    path to the ATLAS style macro
def loadAtlasStyle( path=os.path.join(os.environ.get('HAPPyDIR'), 'ROOT', 'atlasstyle', 'AtlasStyle.C') ):
    import ROOT
    ROOT.gROOT.SetStyle('ATLAS')
    # set the x error size back to the correct value, ATLAS style suggests to show no errors in x direction
    ROOT.gStyle.SetErrorX(0.5)
    ROOT.TGaxis.SetMaxDigits(3)
    setPalette()

## Sets the ROOT color palette from a given RGB list.
#  stops, red, green and blue should all be lists of the same length.
#  The default palette is an improved rainbow palette.
#  In addition a "grayscale" palette is defined.
#  @param name       name to identify the palette
#  @param nContours  number of color steps in the color axis
def setPalette( name='palette', nContours=999 ):
    from ROOT import TColor, gStyle

    if name == "gray" or name == "grayscale":
        stops = [0.00, 0.34, 0.61, 0.84, 1.00]
        red   = [1.00, 0.84, 0.61, 0.34, 0.00]
        green = [1.00, 0.84, 0.61, 0.34, 0.00]
        blue  = [1.00, 0.84, 0.61, 0.34, 0.00]
    # elif name == "whatever":
        # (define more palettes)
    else:
        # default palette, looks cool
        stops = [0.00, 0.34, 0.61, 0.84, 1.00]
        red   = [0.00, 0.00, 0.87, 1.00, 0.51]
        green = [0.00, 0.81, 1.00, 0.20, 0.00]
        blue  = [0.51, 1.00, 0.12, 0.00, 0.00]
    s = array( 'd', stops )
    r = array( 'd', red )
    g = array( 'd', green )
    b = array( 'd', blue )
    # set the actual palette
    nPoints = len( s )
    TColor.CreateGradientColorTable( nPoints, s, r, g, b, nContours )
    gStyle.SetNumberContours( nContours )

## Class to wrap attributes for TAttFill, TAttLine and TAttMarker
#  Allows to define all attributes in a single container and apply them
#  to suitable objects.
class Style:
    ## Default costructor.
    #  @param color         int defining color applied to line, marker and fill if not specified otherwise
    #  @param lineColor     int defining line color (see TAttLine, default is same as color)
    #  @param lineStyle     int defining line style (see TAttLine, default is 0)
    #  @param lineWidth     int defining line width (see TAttLine, default is 3)
    #  @param markerColor   int defining marker color (see TAttMarker, default is same as color)
    #  @param markerStyle   int defining marker style (see TAttMarker, default is 20)
    #  @param markerSize    float defining marker size (see TAttMarker, default is 1.3)
    #  @param fillColor     int defining fill color (see TAttFill, default is same as color)
    #  @param fillStyle     int defining fill style (see TAttFill, default is 0)
    def __init__( self, color=1, lineColor=None, lineStyle=0, lineWidth=3, markerColor=None, markerStyle=20, markerSize=1.2, fillColor=None, fillStyle=0 ):
        markerColor = markerColor if markerColor is not None else color
        lineColor = lineColor if lineColor is not None else color
        fillColor = fillColor if fillColor is not None else color
        ## int defining line color (see https://root.cern.ch/doc/master/classTAttLine.html)
        self.lineColor = lineColor
        ## int defining line style (see https://root.cern.ch/doc/master/classTAttLine.html)
        self.lineStyle = lineStyle
        ## int defining line width (see https://root.cern.ch/doc/master/classTAttLine.html)
        self.lineWidth = lineWidth
        ## int defining fill color (see https://root.cern.ch/doc/master/classTAttFill.html)
        self.fillColor = fillColor
        ## int defining fill style (see https://root.cern.ch/doc/master/classTAttFill.html)
        self.fillStyle = fillStyle
        ## int defining marker color (see https://root.cern.ch/doc/master/classTAttMarker.html)
        self.markerColor = markerColor
        ## int defining marker style (see https://root.cern.ch/doc/master/classTAttMarker.html)
        self.markerStyle = markerStyle
        ## float defining marker size (see https://root.cern.ch/doc/master/classTAttMarker.html)
        self.markerSize = markerSize
    
    ## Constructor from an XML element.
    #  @code
    #  <Style color="" lineColor="" lineStyle="" lineWidth="" markerColor="" markerStyle="" markerSize="" fillColor="" fillStyle=""/>
    #  @endcode
    #  @param element    the XML element
    #  @return new Style object
    @classmethod
    def fromXML( cls, element ):
        attributes = element.attrib
        color = int(attributes['color']) if attributes.has_key( 'color' ) else 1
        style = cls( color )
        if attributes.has_key( 'lineColor' ):
            style.lineColor = int(attributes['lineColor'])
        if attributes.has_key( 'lineStyle' ):
            style.lineStyle = int(attributes['lineStyle'])
        if attributes.has_key( 'lineWidth' ):
            style.lineWidth = int(attributes['lineWidth'])
        if attributes.has_key( 'markerColor' ):
            style.markerColor = int(attributes['markerColor'])
        if attributes.has_key( 'markerStyle' ):
            style.markerStyle = int(attributes['markerStyle'])
        if attributes.has_key( 'markerSize' ):
            style.markerSize = float(attributes['markerSize'])
        if attributes.has_key( 'fillColor' ):
            style.fillColor = int(attributes['fillColor'])
        if attributes.has_key( 'fillStyle' ):
            style.fillStyle = int(attributes['fillStyle'])
        return style

    ## Constructor from another ROOT object.
    #  Copies all relevant attributes.
    #  @param obj    an object that implements TAttLine, TAttMarker and/or TAttFill
    #  @return new Style object
    @classmethod
    def fromObject( cls, obj ):
        style = cls()
        try:
            style.lineColor = obj.GetLineColor()
            style.lineStyle = obj.GetLineStyle()
            style.lineWidth = obj.GetLineWidth()
        except AttributeError:
            pass
        try:
            style.fillColor = obj.GetFillColor()
            style.fillStyle = obj.GetFillStyle()
        except AttributeError:
            pass
        try:
            style.markerColor = obj.GetMarkerColor()
            style.markerStyle = obj.GetMarkerStyle()
            style.markerSize = obj.GetMarkerSize()
        except AttributeError:
            pass
        return style
    
    def __repr__( self ):
        return 'Style(%s, %s, %s, %s, %s, %s, %s, %s, %s)' % (self.lineColor, self.lineColor, self.lineStyle, self.lineWidth, self.markerColor, self.markerStyle, self.markerSize, self.fillColor, self.fillStyle)

    ## Applies its attributes to the given object.
    #  @param obj       the object to modify
    def applyTo( self, obj ):
        if not obj:
            return
        try:
            obj.SetLineColor( self.lineColor )
            obj.SetLineStyle( self.lineStyle )
            obj.SetLineWidth( int(self.lineWidth) )
        except AttributeError:
            pass
        try:
            obj.SetFillColor( self.fillColor )
            obj.SetFillStyle( self.fillStyle )
        except AttributeError:
            pass
        try:
            obj.SetMarkerColor( self.markerColor )
            obj.SetMarkerStyle( self.markerStyle )
            obj.SetMarkerSize( self.markerSize )
        except AttributeError:
            pass

## Version of Style that generates a filled Style by default.
class FilledStyle( Style ):
    ## Default costructor.
    #  Produces a filled style with a thin outline in a different color (gray by default).
    #  @param color         int defining the fill color
    #  @param lineColor     int defining line color (see TAttLine, default is 922)
    #  @param fillStyle     int defining fill style (see TAttFill, default is 1001)
    def __init__( self, color=1, lineColor=922, fillStyle=1001 ):
        Style.__init__( self, color, lineColor=lineColor, fillStyle=fillStyle )

## Create a copy of a Style object with a modified line style.
#  By default changes the linestyle to dashed.
#  @param style       Style to be copied
#  @param lineStyle   line style of the copied object
#  @return new Style object
def dashedLineStyle( style, lineStyle=2 ):
    newStyle.lineStyle = lineStyle
    return newStyle

## Create a copy of a Style object with a modified marker style.
#  Changes marker styles 20, 21, 22 by +4 which replaces filled
#  markers by open markes.
#  @param style      Style to be copied
#  @return new Style object
def openMarkerStyle( style ):
    from ROOT import kDashed
    newStyle = copy.copy( style )
    if newStyle.markerStyle in [20,21,22]:
        newStyle.markerStyle += 4
    return newStyle

#######################
# some default styles #
#######################
mcErrorStyle = Style( 1031, lineColor=0, lineWidth=0, markerStyle=0, fillStyle=3144 )

blackLine = Style( color=1, markerStyle=20 )
redLine = Style( color=632, markerStyle=20 )
orangeLine = Style( color=801, markerStyle=20 )
blueLine = Style( color=600, markerStyle=20 )
greenLine = Style( color=417, markerStyle=20 )

