## @package happy.analysisTools
#  @brief Helper methods for Dataset classes.
#
#  @author Christian Grefe, Bonn University (christian.grefe@cern.ch)
from happy.cut import Cut
from happy.variable import createCutFlowVariable
from happy.dataset import Dataset, CombinedDataset, RootDataset
from happy.systematics import SystematicsSet
from happy.histogramTools import histToGraph
from happy.stringTools import appendUUID
from glob import glob
import os, logging, shutil, math

## Helper method to create a cutflow histogram. Stacked if a luminosity scaling is requested
#  @param datasets       list of datasets to include
#  @param cuts           list of cuts that define the cutflow
#  @param luminosity     luminosity to scale to
#  @param ignoreWeights  set if all weights should be ignored
#  @return the plot object
def createCutflowPlot( datasets, cuts, luminosity=None, ignoreWeights=False ):
    var = createCutFlowVariable( cuts=cuts )
    from happy.plot import Plot
    p = Plot( 'Cutflow', var )
    p.showBinWidthY = False
    if luminosity:
        p.titles.append( '#scale[0.5]{#int}Ldt = %.2g fb^{-1}' % (luminosity / 1000.) )
    for dataset in datasets:
        p.addHistogram( dataset.getCutflowHistogram(cuts, luminosity, ignoreWeights), stacked=luminosity is not None )
    return p

## Helper method to persist dataset definition in a text file
#  @param fileName    name of the output text file
#  @param datasets    list of datasets to persist
def writeDatasetsToTextFile( fileName, datasets ):
    f = open( fileName, 'w' )
    f.write( '# Datasets\n' )
    f.write( '# name; title; treeName; color; crossSection; kFactor; fileName1, fileName2, ...\n' )
    for dataset in datasets:
        f.write( dataset.toString() + '\n' )
    f.close()

## Helper method to read dataset definitions from a text file
#  @param fileName    name of the input text file
#  @param cls         class to use to interpret the text file
#  @return list of datasets
def readDatasetsFromTextFile( fileName, cls=Dataset ):
    f = open( fileName )
    datasets = []
    for line in f:
        line = line.lstrip().rstrip()
        if not line or line[0] in ['#', '/']:
            continue
        datasets.append( cls.fromString( line ) )
    f.close()
    return datasets

## Helper method to persist physics processes definition in a text file
#  @param fileName    name of the output text file
#  @param datasets    list of physics processes to persist
def writePhysicsProcessesToTextFile( fileName, physicsProcesses ):
    f = open( fileName, 'w' )
    f.write( '# PhysicsProcesses\n' )
    f.write( '# name; title; color; kFactor; dataset1, dataset2, ...\n' )
    for physicsProcess in physicsProcesses:
        f.write( physicsProcess.toString() + '\n' )
    f.close()

## Helper method to read physics process definitions from a text file
#  @param fileName    name of the input text file
#  @param cls         class to use to interpret the text file
#  @return list of physics processes
def readPhysicsProcessesFromFile( fileName, cls=CombinedDataset ):
    f = open( fileName )
    physicsProcesses = []
    for line in f:
        line = line.lstrip().rstrip()
        if not line or line[0] in ['#', '/']:
            continue
        physicsProcesses.append( cls.fromString( line ) )
    f.close()
    return physicsProcesses

## Helper method to skim, slim and thin ROOT files
#  If an output file already exists and it is newer than its
#  corresponding input file the skimming will not be performed.
#  @param inputPath    path to look for input files
#  @param pattern      file name pattern used to identify datasets in the input path
#  @param outputPath   path were the output dataset will be created
#  @param selection    Cut object describing the event selection used for the skimming by default all events are kept
#  @param branches     list of strings that define the branches to be kept, if it is empty all branches are kept
#  @param systematics  SystematicsSet to define the systematic variations to be kept
#  @param recreate     force writing of the output files, regardless if they already exist (default = False)
def skimNTuple(inputPath, pattern, outputPath, selection=Cut(), branches=[], systematics=None, recreate=False):
    for datasetPath in glob(os.path.join(inputPath, pattern)):
        datasetName = os.path.split(datasetPath)[1]
        project, dsid, title, derivation, tags, version = datasetName.split('.')[3:9]
        inputFileNamePattern = os.path.join(datasetPath, '*.root*')
        dataset = RootDataset(dsid + '_' + tags, datasetName, [ inputFileNamePattern ])
        if systematics:
            dataset.systematicsSet |= systematics
        # clean output directory
        outputDirectory = os.path.join(outputPath, datasetName)
        outputFileName = os.path.join(outputDirectory, datasetName + '.root')
        if os.path.exists(outputDirectory):
            # check if old output is newer than input file
            if os.path.exists(outputFileName):
                outputChangeTime = os.path.getctime(outputFileName)
                newInput = False
                for f in glob(inputFileNamePattern):
                    if os.path.getctime(f) > outputChangeTime:
                        newInput = True
                        break
                if not newInput and not recreate:
                    logging.info('Skipping %r -- already processed' % dataset)
                    continue
            shutil.rmtree(outputDirectory)
        os.mkdir(outputDirectory)
        # write the slimmed version of the dataset
        dataset.save(outputFileName, selection.cut, branches)

## Creates a graph similar to Dataset.getHistogram. The errors are determined from the combined systematic uncertainties.
#  Loops over all systematics and determines the respective uncertainties.
#  Correlations between all datasets are taken into account by summing over the same variation in all datasets.
#  Possible correlations between different systematics are not taken into account, they are added in quadrature.
#  @param datasets             list of Dataset objects to consider
#  @param xVar                 Variable object that defines the variable expresseion used in draw and the binning
#  @param cut                  Cut object that defines the applied cut
#  @param luminosity           global scale factor, i.e. integrated luminosity, not applied for data
#  @param recreate             force recreation of the histogram (don't read it from a possible histogram file)
#  @return graph
def calculateSystematicsGraph( datasets, xVar, cut=None, luminosity=1., recreate=False ):
    from ROOT import TGraphAsymmErrors
    # get the total nominal histogram
    nominalHist = None
    systematicsSet = SystematicsSet()
    for dataset in datasets:
        systematicsSet |= dataset.combinedSystematicsSet
        h = dataset.getHistogram( xVar, cut, luminosity, recreate=recreate )
        if nominalHist:
            nominalHist.Add( h )
        else:
            nominalHist = h
        
    # convert nominal histogram to a graph
    graph = histToGraph( nominalHist, appendUUID( 'syst' ) , False )
    graph.SetTitle( 'Uncertainty' )
    for systematics in systematicsSet:
        # TODO: instead of simply summing up in quadrature we could include correlation terms (at least within each bin)
        upHist = None
        downHist = None
        # collect all up and down histograms for all datasets
        for dataset in datasets:
            upH = dataset.getHistogram( xVar, cut, luminosity, systematicVariation=systematics.up, recreate=recreate )
            downH = dataset.getHistogram( xVar, cut, luminosity, systematicVariation=systematics.down, recreate=recreate )
            if upH:
                if upHist:
                    upHist.Add( upH )
                else:
                    upHist = upH
            if downH:
                if downHist:
                    downHist.Add( downH )
                else:
                    downHist = downH
        for i in xrange( nominalHist.GetNbinsX() ):
            upVal = upHist.GetBinContent( i + 1 )
            downVal = downHist.GetBinContent( i + 1 )
            nominalVal = nominalHist.GetBinContent( i + 1 )
            deltaUp = upVal - nominalVal
            deltaDown = downVal - nominalVal
            # check which value is the down fluctuation
            if deltaDown > deltaUp:
                deltaDown, deltaUp = deltaUp, deltaDown
            # add uncertainty in quadrature to previous total uncertainties
            graph.SetPointEYlow( i, math.sqrt( graph.GetErrorYlow( i ) ** 2 + deltaDown ** 2 ) )
            graph.SetPointEYhigh( i, math.sqrt( graph.GetErrorYhigh( i ) ** 2 + deltaUp ** 2 ) )
    return graph
