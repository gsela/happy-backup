## @package happy.histogramStore
#  @brief Classes to store and retrieve histograms from a ROOT file via canonical paths.
#
#  @author Christian Grefe, Bonn University (christian.grefe@cern.ch)

import logging, os
from happy.cut import Cut
from happy.miscellaneous import Enum
from happy.stringTools import appendUUID

## Enum of possible histogram lookup modes: HASH and NAME
MODES = Enum( ['HASH', 'NAME'] )

## Class to persist and retrieve histograms based on canonical path names.
#  Supports storing of TH1 and TH2 objects and derived classes.
#  Histograms are stored based on their corresponding Dataset, Variable,
#  SystematicVariation, Cut and a second Cut for the weight expression.
#  TH2 objects require an additional Variable.
#
#  The path of of each object in the ROOT file is either "/Variable/Cut/Dataset/"
#  for 1D objects or "/Variable/Variable/Cut/Dataset/" for 2D objects. The object
#  name is SystematicVariation__Cut, where Cut represents the weight expression.
#
#  Two modes are supported. In the "NAME" mode the lookup is done by the name
#  attribute of the respective object which results in human readable ROOT files
#  but does not guarantee that the lookup is unique. The "HASH" mode uses the
#  md5 attribute of the object which identifies the objects in a truly unique way,
#  i.e. a Cut is defined by its full cut expression and not just its name. The
#  HASH mode is the recommended one.
#
#  Common problems when using the NAME mode are that two identical Variables
#  with different binning can not be distinguished. A modified Cut expression
#  without changing the name can not be distinguished from the previous version.
#  Make use of the recreate argument in Dataset::getHistogram to enforce overriding
#  of the histogram in the HistogramStore.
class HistogramStore( object ):
    _logger = logging.getLogger( __name__ + '.HistogramStore' )
    
    ## Default contructor.
    #  @param fileName      file name of the ROOT file to store the histograms
    #  @param mode          histogram lookup mode (default is HASH)
    def __init__( self, fileName, mode=MODES.HASH ):
        
        ## File name of the ROOT file
        self.fileName = fileName
        ## Lookup mode used (default is lookup by hash)
        self.mode = mode
        
        # book keeping for opened files
        self._openFiles = {}
        # currently used file
        self._file = None
    
    ## Create a new HistogramStore object from an XML element.
    #  @code
    #  <HistogramStore> fileName </HistogramStore>
    #  @endcode
    #  @param element   XML element
    #  @return HistogramStore
    @classmethod
    def fromXML( cls, element ):
        return cls( element.text.strip() )
    
    def __repr__( self ):
        return 'HistogramStore("%s","%s")' % ( self.fileName, self.mode )
    
    def __str__( self ):
        return 'HistogramStore(file:"%s", mode:"%s")' % ( self.fileName, self.mode )
    
    ## Closes the underlying ROOT files
    def close( self ):
        for f in self._openFiles.values():
            if f and f.IsOpen():
                f.Close()
            if self._file is f:
                self._file = None
    
    ## open the underlying ROOT file
    def _openFile( self, fileName, mode='read' ):
        if self._file and self._file.IsOpen() and self._file.GetName() == fileName:
            if self._file.ReOpen( mode ) > -1:
                return True
            else:
                return False
        if self._openFiles.has_key( fileName ):
            self._file = self._openFiles[ fileName ]
            if self._file and self._file.IsOpen():
                return True
        from ROOT import TFile
        # stupid ROOT does not let us supress error messages here :(
        self._file = TFile.Open( fileName, mode )
        if not self._file:
            # the file does not exist, try to create it
            self._file = TFile.Open( fileName, 'create' )
            self._logger.info( '_open(): created HistogramStore file at "%s"' % fileName )
        if self._file and self._file.IsOpen():
            self._openFiles[ fileName ] = self._file
            self._logger.debug( '_open(): opened file at "%s"' % fileName )
            return True
        self._logger.warning( '_open(): unable to open file at "%s"' % fileName )
        return False
    
    ## Open the underlying ROOT file depending on the histogram.
    #  @param dataset              Dataset object
    #  @param systematicVariation  SystematicVariation object
    #  @param xVar                 Variable object
    #  @param yVar                 Variable object
    #  @param cut                  Cut object
    #  @param weightExpression     Cut object representing the total weight expression
    #  @param mode                 ROOT file mode: default is "read"
    #  @return bool -- opening was successful
    def _open( self, dataset, systematicVariation, xVar, yVar=None, cut=Cut(), weightExpression=Cut(), mode='read' ):
        return self._openFile( self.fileName, mode )
        
    ## Helper method to generate canonical path.
    #  @param dataset              Dataset object
    #  @param systematicVariation  SystematicVariation object
    #  @param xVar                 Variable object
    #  @param yVar                 Variable object
    #  @param cut                  Cut object
    #  @param weightExpression     Cut object
    #  @return (string, string) -- path and histogram name (tupel of size 2)
    def _buildPath( self, dataset, systematicVariation, xVar, yVar=None, cut=Cut(), weightExpression=Cut() ):
        if self.mode == MODES.HASH:
            histName = systematicVariation.md5
            if yVar:
                path = os.path.join( xVar.md5, yVar.md5, cut.md5, dataset.md5 )
            else:
                path = os.path.join( xVar.md5, cut.md5, dataset.md5 )

            if weightExpression is not Cut():
                histName += '__%s' % weightExpression.md5

        elif self.mode == MODES.NAME:
            histName = systematicVariation.name
            if yVar:
                path = os.path.join( xVar.name, yVar.name, cut.name, dataset.name )
            else:
                path = os.path.join( xVar.name, cut.name, dataset.name )
 
            if weightExpression:
                histName += '_%s' % weightExpression.cut

        else:
            self._logger.error( '_buildPath: unsuported mode "%s"' % self.mode )
            raise AttributeError

        return path, histName
    
    ## Helper method to generate canonical path to directory containing systematic variations.
    #  @param dataset              Dataset object
    #  @param xVar                 Variable object
    #  @param yVar                 Variable object
    #  @param cut                  Cut object
    #  @param weightExpression     Cut object representing the total weight expression
    #  @return string -- path
    def _buildPathToSystematics( self, dataset, xVar, yVar=None, cut=Cut(), weightExpression=Cut() ):
        if self.mode == MODES.HASH:
            if yVar:
                path = os.path.join( xVar.md5, yVar.md5, cut.md5, dataset.md5 )
            else:
                path = os.path.join( xVar.md5, cut.md5, dataset.md5 )
        elif self.mode == MODES.NAME:
            if yVar:
                path = os.path.join( xVar.name, yVar.name, cut.name, dataset.name )
            else:
                path = os.path.join( xVar.name, cut.name, dataset.name )
        else:
            self._logger.error( '_buildPath: unsuported mode "%s"' % self.mode )
            raise AttributeError
        return path
    
    ## Determines the list of available SystematicVariation objects.
    #  @param dataset              Dataset object
    #  @param var                  Variable object
    #  @param cut                  Cut object
    #  @return list -- list of systematic variation names
    def getSystematicVariationNames( self, dataset, var, cut ):
        result = set()
        if self._open( dataset, None, var, cut ):
            directory = self._file.Get( self._buildPathToSystematics( dataset, var, cut=cut ) )
            for key in directory.GetListOfKeys():
                result.add( key.GetName() )
        return result
    
    ## Get a 1D histogram from the store.
    #  @param dataset              Dataset object
    #  @param systematicVariation  SystematicVariation object
    #  @param var                  Variable object
    #  @param cut                  Cut object
    #  @param weightExpression     Cut object representing the total weight expression
    #  @return TH1
    def getHistogram( self, dataset, systematicVariation, var, cut=Cut(), weightExpression=Cut() ):
        if not self._open( dataset, systematicVariation, var, cut=cut, weightExpression=weightExpression ):
            return None
        path, histogramName = self._buildPath( dataset, systematicVariation, var, cut=cut, weightExpression=weightExpression )
        histPath = os.path.join( path, histogramName )
        h = self._file.Get( histPath )
        if h:
            h = h.Clone( appendUUID( histogramName ) )
            h.SetDirectory( 0 )
            self._logger.debug( 'getHistogram(): found histogram "%s"' % histPath )
        else:
            self._logger.debug( 'getHistogram(): could not find histogram "%s"' % histPath )
        return h
    
    ## Put a 1D histogram into the store.
    #  @param dataset              Dataset object
    #  @param systematicVariation  SystematicVariation object
    #  @param var                  Variable object
    #  @param cut                  Cut object
    #  @param weightExpression     Cut object representing the total weight expression
    #  @param histogram            TH1 object
    def putHistogram( self, dataset, systematicVariation, var, cut=Cut(), weightExpression=Cut(), histogram=None ):
        if not histogram:
            return
        if not self._open( dataset, systematicVariation, var, cut=cut, weightExpression=weightExpression, mode='update' ):
            self._logger.warning( 'putHistogram(): unable to store histogram, file not open' )
            return
        path, histogramName = self._buildPath( dataset, systematicVariation, var, cut=cut, weightExpression=weightExpression )
        self._logger.debug( 'putHistogram(): storing histogram "%s/%s"' % ( path, histogramName ) )
        self._file.cd()
        from ROOT import TObject, gDirectory
        for directory in path.split( '/' ):
            if not gDirectory.GetDirectory( directory ):
                gDirectory.mkdir( directory )
            gDirectory.cd( directory )
        histogram = histogram.Clone( histogramName )
        histogram.Write( histogramName, TObject.kOverwrite )
    
    ## Get a 2D histogram from the store.
    #  @param dataset              Dataset object
    #  @param systematicVariation  SystematicVariation object
    #  @param xVar                 Variable object
    #  @param yVar                 Variable object
    #  @param cut                  Cut object
    #  @param weightExpression     Cut object representing the total weight expression
    #  @return TH2
    def getHistogram2D( self, dataset, systematicVariation, xVar, yVar, cut=Cut(), weightExpression=Cut() ):
        if not self._open( dataset, systematicVariation, xVar, yVar, cut, weightExpression ):
            return None
        path, histogramName = self._buildPath( dataset, systematicVariation, xVar, yVar, cut, weightExpression )
        histPath = os.path.join( path, histogramName )
        h = self._file.Get( histPath )
        if h:
            h = h.Clone( appendUUID( histogramName ) )
            h.SetDirectory( 0 )
            self._logger.debug( 'getHistogram(): found histogram "%s"' % histPath )
        else:
            self._logger.debug( 'getHistogram(): could not find histogram "%s"' % histPath )
        return h
    
    ## Put a 2D histogram into the store.
    #  @param dataset              Dataset object
    #  @param systematicVariation  SystematicVariation object
    #  @param xVar                 Variable object
    #  @param yVar                 Variable object
    #  @param cut                  Cut object
    #  @param weightExpression     Cut object representing the total weight expression
    #  @param histogram            TH2 object
    def putHistogram2D( self, dataset, systematicVariation, xVar, yVar, cut=Cut(), weightExpression=Cut(), histogram=None ):
        if not histogram:
            return
        if not self._open( dataset, systematicVariation, xVar, yVar, cut, weightExpression, mode='update' ):
            self._logger.error( 'putHistogram(): unable to store histogram, can not open "%s"' % self.fileName )
            return
        path, histogramName = self._buildPath( dataset, systematicVariation, xVar, yVar, cut, weightExpression )
        self._logger.debug( 'putHistogram(): storing histogram "%s/%s"' % ( path, histogramName ) )
        self._file.cd()
        from ROOT import TObject, gDirectory
        for directory in path.split( '/' ):
            if not gDirectory.GetDirectory( directory ):
                gDirectory.mkdir( directory )
            gDirectory.cd( directory )
        histogram = histogram.Clone( histogramName )
        histogram.Write( histogramName, TObject.kOverwrite )

 
## Class to persist and retrieve histograms based on canonical path names.
#  The expected file structure matches the one used for the "WorkspaceBuiler",
#  which expects "/Cut/Dataset/" as path and "SystematicVariation" as object name
#  and contains only histograms for a single Variable. Unlike HistogramStore only
#  NAME lookup is supported. Multiple ROOT files can be defined to support lookup
#  for multiple variables. The second Cut object to identify the weight expression is
#  ignored.
class WorkspaceInputHistogramStore( HistogramStore ):
    _logger = logging.getLogger( __name__ + '.WorkspaceInputHistogramStore' )
    
    ## Default contructor 
    def __init__( self ):
        HistogramStore.__init__( self, None, MODES.NAME )
        
        # mapping of variables to file names
        self._fileDict = {}
    
    ## Constructor from an XML element.
    #  @code
    #  <MultiFileHistogramStore> </MultiFileHistogramStore>
    #  @endcode
    #  @param element    the XML element
    #  @return the HistogramStore object
    @classmethod
    def fromXML( cls, element ):
        return cls( element.text.strip() )
    
    ## Return the canonical string representation
    def __repr__( self ):
        return 'WorkspaceInputHistogramStore(%r)' % self._fileDict
    
    ## Sets the ROOT file to be used for the given variable
    #  @param var        Variable object
    #  @param fileName   path to the ROOT file
    def setFile( self, var, fileName ):
        if self._openFile( fileName ):
            if not self._fileDict.has_key( var.name ):
                self._fileDict[ var.name ] = {}
            for key in self._file.GetListOfKeys():
                if key.IsFolder():
                    self._fileDict[ var.name ][ key.GetName() ] = fileName
    
    ## Open the underlying ROOT file depending on the histogram.
    #  @param dataset              Dataset object
    #  @param systematicVariation  SystematicVariation object
    #  @param xVar                 Variable object
    #  @param yVar                 Variable object
    #  @param cut                  Cut object
    #  @param weightExpression     Cut object representing the total weight expression
    #  @param mode                 ROOT file mode: default is "read"
    #  @return if opening was successful
    def _open( self, dataset, systematicVariation, xVar, yVar=None, cut=Cut(), weightExpression=Cut(), mode='read' ):
        if self._fileDict.has_key( xVar.name ):
            return self._openFile( self._fileDict[ xVar.name ][ cut.name ], mode )
        else:
            self._logger.warning( '_open(): no file defined for "%s"' % xVar )
            return False
    
    ## helper method to generate canonical path
    #  @param dataset              Dataset object
    #  @param systematicVariation  SystematicVariation object
    #  @param xVar                 Variable object
    #  @param yVar                 Variable object
    #  @param cut                  Cut object
    #  @param weightExpression     Cut object representing the total weight expression
    #  @return the path and histogram name (tupel of size 2)
    def _buildPath( self, dataset, systematicVariation, xVar, yVar=None, cut=Cut(), weightExpression=Cut() ):
        histName = systematicVariation.name
        if yVar:
            path = os.path.join( yVar.name, cut.name, dataset.name )
        else:
            path = os.path.join( cut.name, dataset.name )
        return path, histName
    
    ## Helper method to generate canonical path to directory containing systematic variations.
    #  @param dataset              Dataset object
    #  @param xVar                 Variable object
    #  @param yVar                 Variable object
    #  @param cut                  Cut object
    #  @param weightExpression     Cut object representing the total weight expression
    #  @return the path
    def _buildPathToSystematics( self, dataset, xVar, yVar=None, cut=Cut(), weightExpression=Cut() ):
        return os.path.join( cut.name, dataset.name )

def testHistogramStore():
    from happy.dataset import Dataset
    from happy.variable import Binning, Variable
    from happy.systematics import nominalSystematics as s
    # create some required objects
    v = Variable( 'tau_pt', title='p_{T}^{#tau}', unit='GeV', binning=Binning( 20, -3, 3 ) )
    histogramTitle = 'TestHistogram'
    h = v.createHistogram( 'TestHistogram' )
    h.FillRandom( 'gaus', 1000 )
    c = Cut( 'signal_region', cut = '|tau_eta| < 2.5' )
    w = Cut( 'weight_total' )
    d = Dataset( 'test_dataset' )
    # create a histogram store
    storeFileName = 'testHistogramStore.root'
    store = HistogramStore( storeFileName )
    # store objects by hash value instead of names
    store.useHash = True
    # store a histogram
    store.putHistogram( d, s, v, c, w, h )
    # close the store
    store.close()    
    # retrieve a histogram (automatically opens the file again)
    h = store.getHistogram( d, s, v, c, w )
    # clean up
    store.close()
    os.remove( storeFileName )
    # check if everything worked and the histograms are actually the same
    return h.GetTitle() == histogramTitle

if __name__ == '__main__':
    pass
    #print testHistogramStore()
        
