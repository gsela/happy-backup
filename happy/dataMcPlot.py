## @package happy.dataMcPlot
#  @brief Package to handle data-MC plots.
#
#  @author Christian Grefe, Bonn University (christian.grefe@cern.ch)
from happy.plot import Plot
from happy.variable import var_Events
from happy.histogramTools import histToGraph, combineStatsAndSystematics, normaliseGraphToBinContent, addGraphs, resetGraph
from happy.stringTools import appendUUID
from copy import copy

## Plot that shows signal, background and data as well as combined background uncertainty.
#  @include dataMcPlot.py
#  @image html dataMcPlot.png
class DataMcPlot( Plot ):
    from happy.style import Style
    from ROOT import TColor
    defaultStatErrorTitle = 'Stat. Uncert.'
    defaultSystErrorTitle = 'Syst. Uncert.'
    defaultCombinedErrorTitle = 'Stat #oplus Syst'
    defaultStatErrorStyle = Style( TColor.GetColorTransparent( 820, 0.3 ), lineWidth=0, fillStyle=1000, markerStyle=0 )
    defaultSystErrorStyle = Style( 923, lineWidth=0, fillStyle=3354, markerStyle=0 )
    defaultCombinedErrorStyle = Style( 923, lineWidth=0, fillStyle=3354, markerStyle=0 )
    
    ## Default constructor.
    #  @param title       string used as title of the TCanvas
    #  @param variableX   Variable object defining the x axis
    #  @param variableY   Variable object defining the y axis (optional, default uses "Events")
    def __init__( self, title='DataMcPlot', variableX=None, variableY=var_Events ):
        Plot.__init__( self, title, variableX, variableY )
        
        # copy settings from default settings 
        ## Style used for the statistical uncertainty band
        self.statErrorStyle = copy( self.defaultStatErrorStyle )
        ## Style used for the systematic uncertainty band
        self.systErrorStyle = copy( self.defaultSystErrorStyle )
        ## Style used for the combined uncertainty band 
        self.combinedErrorStyle = copy( self.defaultCombinedErrorStyle )
        ## Legend title used for the statistical uncertainty
        self.statErrorTitle = copy( self.defaultStatErrorTitle )
        ## Legend title used for the systematic uncertainty
        self.systErrorTitle = copy( self.defaultSystErrorTitle )
        ## Legend title used for the combined uncertainty
        self.combinedErrorTitle = copy( self.defaultCombinedErrorTitle )
        
        # objects with special roles
        ## List of signal histograms
        self.signalHistograms = []
        ## Graph that represents the data
        self._dataGraph = None
        ## Graph that represents the combined uncertainty of the total background
        self._mcCombinedErrorGraph = None
        ## Graph that represents the systematic uncertainty of the total background
        self._mcSystGraph = None
        ## Histogram with the total entries of all backgrounds
        self._mcSumHist = None
        ## Histogram with the total entries of all backgrounds without uncertainties
        self._mcSumHistWithoutErrors = None
        
        # flags to decide which errors bars are shown
        ## Decide if the statistical uncertainties should be drawn
        self.drawStatError = False
        ## Decide if the systematic uncertainties should be drawn
        self.drawSystError = False
        ## Decide if the combined uncertainties should be drawn
        self.drawCombinedError = True
        ## Decide if the error band should be normalised to the sum of background events per bin
        self.normaliseErrorToBackground = False
    
    def remove(self, obj):
        Plot.remove( self, obj )
        # make sure the object is also removed from the list of signal histograms
        while obj in self.signalHistograms:
            self.signalHistograms.remove( obj )
    
    ## Adds a histogram to the plot.
    #  @param histogram    TH1 object
    #  @param drawOption   option to draw histogram (default is "HIST")
    #  @param stacked      decide if histogram is included in stack or drawn separately (default is "True")
    #  @param copy         decide if histogram should be copied internally. Use if same object is modified for another plot (default is "False")
    #  @return histogram object (copied version if copy=True)
    def addHistogram( self, histogram, drawOption='HIST', stacked=True, copy=False ):
        if not histogram:
            return
        # create a histogram that will be used to store the sum of all background histograms
        if not self._mcSumHist:
            self._mcSumHist = histogram.Clone( appendUUID( self.title + '_McSumHist' ) )
            if not self._mcSumHist.GetSumw2N():
                self._mcSumHist.Sumw2()
            self._mcSumHistWithoutErrors = histogram.Clone( appendUUID( self.title + '_McSumHistWithoutErrors' ) )
        return Plot.addHistogram( self, histogram, drawOption, stacked, copy )
    
    ## Adds a signal histogram to the plot.
    #  Signal histograms are plottet like other histograms, but are only excluded for total background calculation
    #  @param histogram    TH1 object
    #  @param drawOption   option to draw histogram (default is "HIST")
    #  @param stacked      decide if histogram is included in stack or drawn separately (default is "False")
    #  @param copy         decide if histogram should be copied internally. Use if same object is modified for another plot (default is "False")
    #  @return histogram object (copied version if copy=True)
    def addSignalHistogram( self, histogram, drawOption='HIST', stacked=False, copy=False ):
        if not histogram:
            return
        histogram = self.addHistogram( histogram, drawOption, stacked, copy )
        self.signalHistograms.append( histogram )
        return histogram
    
    ## Sets the graph with combined systematic uncertainty, needs to be provided externally
    #  @param graph    TGraph object
    def setMCSystematicsGraph( self, graph ):
        if not graph:
            return
        self._mcSystGraph = graph.Clone( appendUUID( self.title + '_McSystGraph' ) )
        self._mcCombinedErrorGraph = graph.Clone( appendUUID( self.title + '_McCombinedErrorGraph' ) )
    
    ## Sets the data histogram
    #  the histogram is converted implicitely to TGraphAsymmErrors.
    #  By default the uncertianty is calculated as Poisson errors, assuming that entries are unweighted.
    #  Use setDataGraph to pass a user defined TGraph directly.
    #  @param histogram      TH1 object
    #  @param poissonErrors  determine bin errors as Poisson errors from the bin content
    def setDataHistogram( self, histogram, poissonErrors=True ):
        if not histogram:
            return
        # convert to TGraphAsymmErrors to represent correct Poisson errors
        self.setDataGraph( histToGraph( histogram, appendUUID( self.title + '_DataGraph' ), poissonErrors=poissonErrors ) )

    ## Sets the data graph
    #  @param histogram   TGraph object
    #  @param copy        decide if graph should be copied internally. Use if same object is modified for another plot (default is "False")
    def setDataGraph( self, graph, copy=False ):
        if not graph:
            return
        self._dataGraph = graph.Clone( appendUUID( self.title + '_DataGraph' ) )

    def __initAction__( self ):
        Plot.__initAction__( self )
        # update style and titles of special objects
        if self._mcSumHist:
            self.statErrorStyle.applyTo( self._mcSumHist )
            self._mcSumHist.SetTitle( self.statErrorTitle )
        if self._mcSystGraph:  
            self.systErrorStyle.applyTo( self._mcSystGraph )
            self._mcSystGraph.SetTitle( self.systErrorTitle )
        if self._mcCombinedErrorGraph:
            self.combinedErrorStyle.applyTo( self._mcCombinedErrorGraph )
            self._mcCombinedErrorGraph.SetTitle( self.combinedErrorTitle )
        # remove the histograms / graphs with special roles to add them in correct order later
        # note: the stacked histogram order is defined by the order in which they were added
        for obj in [ self._dataGraph, self._mcSumHist, self._mcSystGraph, self._mcCombinedErrorGraph ]:
            pass
            self.remove( obj )
        # add all uncertainty bands on top of each other: combined -> syst -> stat
        # corresponding legend entries are added to bottom
        if self._mcCombinedErrorGraph:
            resetGraph( self._mcCombinedErrorGraph )
        if self.drawCombinedError and self._mcCombinedErrorGraph:
            self.addGraph( self._mcCombinedErrorGraph, '2' )
        if self.drawSystError and self._mcSystGraph:  
            self.addGraph( self._mcSystGraph, '2' )
        if self.drawStatError and self._mcSumHist:
            Plot.addHistogram( self, self._mcSumHist, 'E2' )
        # add data as last object, markers will be drawn on top. Add it as first legend entry to appear on top of legend
        if self._dataGraph:
            self.addGraph( self._dataGraph, 'P0' )
            self.legendElements.insert( 0, self.legendElements.pop() )
    
    def __mainAction__( self ):
        Plot.__mainAction__( self )
        # build the sum of all background components after potential modifications in main action step.
        if self._mcSumHist:
            self._mcSumHist.Reset()
            self._mcSumHistWithoutErrors.Reset()
            for hist in self.stackedHistograms:
                if hist not in self.signalHistograms:
                    self._mcSumHist.Add( hist )
            self._mcSumHistWithoutErrors.Add( self._mcSumHist )
            for iBin in xrange(self._mcSumHistWithoutErrors.GetNbinsX() + 2):
                self._mcSumHistWithoutErrors.SetBinError( iBin, 0. )
        # update the combined uncertainty
        if self.normaliseErrorToBackground and self._mcSystGraph:
            normaliseGraphToBinContent( self._mcSystGraph, self._mcSumHist )
        if self._mcSystGraph and self._mcCombinedErrorGraph:
            addGraphs( self._mcCombinedErrorGraph, self._mcSystGraph )
        if self._mcSumHist and self._mcCombinedErrorGraph:
            combineStatsAndSystematics( self._mcSumHist, self._mcCombinedErrorGraph )
                
if __name__ == '__main__':
    # run the example
    from examples import dataMcPlot
    
