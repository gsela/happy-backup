## @package happy.plotDecorator
#  @brief Classes for decorating plots with titles, legends and other elements.
#
#  @author Christian Grefe, Bonn University (christian.grefe@cern.ch)
from happy.miscellaneous import Enum
import logging

## Possible relative positions within a canvas of objects created by PlotDecorator objects.
positions = Enum(['LeftTop', 'LeftCenter', 'LeftBottom', 'RightTop', 'RightCenter', 'RightBottom'])

## Class that adds additional objects or text to a BasicPlot based on its attributes.
class PlotDecorator( object ):
    logger = logging.getLogger( __name__ + '.PlotDecorator' )
    ## Default constructor
    #  @param position     string defining the relative position of the created objects (see positions, optional, default is "LeftTop")
    def __init__( self, position=positions.LeftTop ):
        ## String defining the relative position of the created objects
        self.position = position
        ## Float defining the default width of all objects (used to determine placement)
        self.defaultWidth = 0.2
        ## Float defining the default height of all objects (used to determine placement)
        self.defaultHeight = 0.2
        ## Float defining the maximum position in y of all objects (used to determine placement)
        self.defaultUpperY = 0.93
        ## Float defining the minimum position in y of all objects (used to determine placement)
        self.defaultLowerY = 0.2
        # internal store of current x position used for relative placement
        self._currentX = 0.
        # internal store of current y position used for relative placement
        self._currentY = 0.

    ## Main method to create all necessary objects and add them to the given BasicPlot object.
    #  @param plot     BasicPlot object
    def decorate( self, plot ):
        self.__calculateSize__( plot )
        self.__resetBasePosition__( plot )
        # Needs to be implemented in derived classes
    
    # Hook to calculate the total size of all objects after placing them.
    def __calculateSize__( self, plot ):
        self.height = self.defaultHeight
        self.width = self.defaultWidth

    # Hook to reset the current positions to the original positions.
    def __resetBasePosition__( self, plot ):
        offsetX = 0.04 * plot._scaleX
        offsetY = 0.03 * plot._scaleY
        if self.position == positions.LeftTop:
            self._currentX = plot.canvas.GetLeftMargin() + offsetX
            self._currentY = 1. - plot.canvas.GetTopMargin() - offsetY
        elif self.position == positions.RightTop:
            self._currentX = 1. - plot.canvas.GetRightMargin() - offsetX - self.width
            self._currentY = 1. - plot.canvas.GetTopMargin() - offsetY
        elif self.position == positions.LeftCenter:
            self._currentX = plot.canvas.GetLeftMargin() + offsetX
            self._currentY = 0.5*(plot.canvas.GetTopMargin() - plot.canvas.GetBottomMargin() + self.height)
        elif self.position == positions.RightCenter:
            self._currentX = 1. - plot.canvas.GetRightMargin() - offsetX - self.width
            self._currentY = 0.5*(plot.canvas.GetTopMargin() - plot.canvas.GetBottomMargin() + self.height)
        elif self.position == positions.LeftBottom:
            self._currentX = plot.canvas.GetLeftMargin() + offsetX
            self._currentY = plot.canvas.GetBottomMargin() + offsetY + self.height
        elif self.position == positions.RightBottom:
            self._currentX = 1. - plot.canvas.GetRightMargin() - offsetX - self.width
            self._currentY = plot.canvas.GetBottomMargin() + offsetY + self.height
        else:
            self.logger.error( '__resetBasePosition__: unknown position "%s"' %self.position )
            self._currentX = 0.0
            self._currentY = 0.0

## Class that generates a legend for a BasicPlot.
#  Creates one entry for each LegendEntry object in BasicPlot::legendEntries, which are
#  typically all histograms, graphs and functions with a title that is not empty.
#  If the number of entries exceeds maxEntriesPerColumn, the entries are distributed
#  equally over multiple columns. The width of each column is automatically determined
#  from the longest text entry in the respective column.
class LegendDecorator( PlotDecorator ):
    logger = logging.getLogger( __name__ + '.LegendDecorator' )
    ## Default constructor.
    #  @param position     string defining the relative position of the created objects (see positions, optional, default is "RightTop")
    def __init__( self, position=positions.RightTop ):
        PlotDecorator.__init__( self, position )
        ## Additional text that is placed above the legend
        self.labelText = ''
        ## Text size of the additional label text
        self.labelTextSize = 0.05
        ## Font of the additional label text
        self.labelTextFont = 42
        ## Text size used for all legend entries
        self.textSize = 0.04
        ## Font used for all legend entries
        self.textFont = 42
        ## Size of additional vertical space between each line
        self.lineGap = 0.0
        ## Size of additional horizontal space between each column
        self.columnGap = 0.01
        ## Maximum number of legend entries in each column
        self.maxEntriesPerColumn = 6
        ## Width of the object used to indicate the referred object. The height is determined from text size. 
        self.objectWidth = 0.03
        ## Minimum width for each column. The necessary width is determined from the longest entry in the respective column.
        self.minColumnWidth = 0.0
        # internal variables
        self._columnEntries = {}
        self._columnWidth = {}
    
    def decorate( self, plot ):
        PlotDecorator.decorate( self, plot )
        # apply scaling, text sizes are affected by both directions
        lineGap = self.lineGap * plot._scaleY
        textSize = self.textSize * plot._scaleTextX
        labelTextSize = self.labelTextSize * plot._scaleTextX
        objectWidth = self.objectWidth * plot._scaleX
        columnGap = self.columnGap * plot._scaleX
        from ROOT import TLatex, TPave, TLine, TMarker
        # add a label on top
        if self.labelText:
            self._currentY -= labelTextSize + lineGap
            t = TLatex( self._currentX, self._currentY, self.labelText )
            t.SetNDC()
            if labelTextSize:
                t.SetTextSize( labelTextSize )
            if self.labelTextFont:
                t.SetTextFont( self.labelTextFont )
            plot._decoratorObjects.append( t )
        
        # create all legend entries with their referenced objects
        basePositionY = self._currentY
        for column, columnEntries in self._columnEntries.items():
            self._currentY = basePositionY
            for legendElement, legendText in columnEntries:
                self._currentY -= textSize + lineGap
                if legendElement.obj:
                    # add a filled box with fill attributes of the referred object and an outline using the line attributes
                    if 'f' in legendElement.option.lower():
                        o = TPave( self._currentX, self._currentY, self._currentX + objectWidth, self._currentY + 0.7*textSize, 1, 'NDC' )
                        o.SetFillColor( legendElement.obj.GetFillColor() )
                        o.SetFillStyle( legendElement.obj.GetFillStyle() )
                        o.SetLineColor( legendElement.obj.GetLineColor() )
                        o.SetLineWidth( legendElement.obj.GetLineWidth() )
                        o.SetLineStyle( legendElement.obj.GetLineStyle() )
                        plot._decoratorObjects.append( o )
                    # add a horizontal line using the line attributes of the referred object
                    if 'l' in legendElement.option.lower():
                        o = TLine( self._currentX, self._currentY + 0.3*textSize, self._currentX + objectWidth, self._currentY + 0.3*textSize )
                        o.SetLineColor( legendElement.obj.GetLineColor() )
                        o.SetLineStyle( legendElement.obj.GetLineStyle() )
                        o.SetLineWidth( legendElement.obj.GetLineWidth() )
                        o.SetBit( TLine.kLineNDC )
                        plot._decoratorObjects.append( o )
                    # add a vertical line using the line attributes of the referred object
                    if 'e' in legendElement.option.lower():
                        o = TLine( self._currentX + 0.5*objectWidth, self._currentY, self._currentX + 0.5*objectWidth, self._currentY + 0.6*textSize )
                        o.SetLineColor( legendElement.obj.GetLineColor() )
                        o.SetLineStyle( legendElement.obj.GetLineStyle() )
                        o.SetLineWidth( legendElement.obj.GetLineWidth() )
                        o.SetBit( TLine.kLineNDC )
                        plot._decoratorObjects.append( o )
                    # add a marker using the marker attributes
                    if 'p' in legendElement.option.lower():
                        o = TMarker( self._currentX + 0.5*objectWidth, self._currentY + 0.3*textSize, legendElement.obj.GetMarkerStyle() )
                        o.SetMarkerSize( legendElement.obj.GetMarkerSize() )
                        o.SetMarkerColor( legendElement.obj.GetMarkerColor() )
                        o.SetNDC( True )
                        plot._decoratorObjects.append( o )
                legendText.SetY( self._currentY )
                legendText.SetX( self._currentX + objectWidth + 0.3*textSize )
                plot._decoratorObjects.append( legendText )
            self._currentX += self._columnWidth[column] + objectWidth + 0.3*textSize + columnGap
    
    # Hook to calculate width and height of the placed objects
    def __calculateSize__( self, plot ):
        # apply scaling, text sizes are affected by both directions
        lineGap = self.lineGap * plot._scaleY
        textSize = self.textSize *  plot._scaleTextX
        labelTextSize = self.labelTextSize * plot._scaleTextX
        objectWidth = self.objectWidth * plot._scaleX
        columnGap = self.columnGap * plot._scaleX
        minColumnWidth = self.minColumnWidth * plot._scaleX
        nColumns = 1
        # distribute the legend entries equally over multiple columns
        if len(plot.legendElements) > self.maxEntriesPerColumn:
            nColumns = len(plot.legendElements) / int( self.maxEntriesPerColumn )
            if len(plot.legendElements) % int( self.maxEntriesPerColumn ) > 0:
                nColumns += 1
        for column in xrange( nColumns ):
            self._columnEntries[column] = []
            self._columnWidth[column] = minColumnWidth
        currentColumn = 0
        maxColumnEntries = nColumns*[0]
        for legendElement in plot.legendElements:
            maxColumnEntries[ currentColumn ] += 1
            currentColumn += 1
            if currentColumn >= nColumns:
                currentColumn = 0
        import array
        w = array.array( 'I', [0] )
        h = array.array( 'I', [0] )
        currentColumn = 0
        # calculate the required width of each column from its longest entry
        for legendElement in plot.legendElements:
            from ROOT import TLatex
            t = TLatex( 0., 0., legendElement.getText() )
            t.SetTextSize( textSize )
            t.SetTextFont( self.textFont )
            t.SetNDC( True )
            t.Draw()
            t.GetBoundingBox( w, h )
            width = w[0] / float(plot.canvas.GetWw() * plot.canvas.GetWNDC())
            if width > self._columnWidth[currentColumn]:
                self._columnWidth[currentColumn] = width
            self._columnEntries[currentColumn].append( (legendElement, t) )
            if len( self._columnEntries[currentColumn] ) >= maxColumnEntries[currentColumn]:
                currentColumn += 1
        # calculate vertical space
        nLines = len( self._columnEntries[0] )
        self.height = nLines * textSize + (nLines - 1) * self.lineGap
        # add space for the label
        if self.labelText:
            self.height += labelTextSize + lineGap
        # calculate the total width
        self.width = 0.
        for columnWidth in self._columnWidth.values():
            self.width += columnWidth + objectWidth + 0.15*textSize
        self.width += (nColumns - 1) * columnGap

## Class that generates title and labels for a BasicPlot.
#  Creates one text line for each object in BasicPlot::titles.
#  Optionally can add an additional line as label above the title lines.
class TitleDecorator( PlotDecorator ):
    logger = logging.getLogger( __name__ + '.TitleDecorator' )
    ## Default constructor.
    #  @param position     string defining the relative position of the created objects (see positions, optional, default is "LeftTop")
    def __init__( self, position=positions.LeftTop ):
        PlotDecorator.__init__( self, position )
        ## Additional text that is placed above the titles
        self.labelText = ''
        ## Text size of the additional label text
        self.labelTextSize = 0.05
        ## Font of the additional label text
        self.labelTextFont = 42
        ## Text size used for all title lines
        self.textSize = 0.04
        ## Font used for all title lines
        self.textFont = 42
        ## Size of additional vertical space between each line
        self.lineGap = 0.0

    def decorate( self, plot ):
        PlotDecorator.decorate( self, plot )
        # apply scaling, text sizes are affected by both directions
        lineGap = self.lineGap * plot._scaleY
        textSize = self.textSize * plot._scaleTextX
        labelTextSize = self.labelTextSize * plot._scaleTextX
        from ROOT import TLatex
        # add the label text above the titles
        if self.labelText:
            self._currentY -= labelTextSize + lineGap
            t = TLatex( self._currentX, self._currentY, self.labelText )
            t.SetNDC()
            if labelTextSize:
                t.SetTextSize( labelTextSize )
            if self.labelTextFont:
                t.SetTextFont( self.labelTextFont )
            plot._decoratorObjects.append( t )
        # add a line of text for each title defined in the BasicPlot object
        for title in plot.titles:
            self._currentY -= textSize + lineGap
            t = TLatex( self._currentX, self._currentY, title )
            t.SetNDC()
            if textSize:
                t.SetTextSize( textSize )
            if self.textFont:
                t.SetTextFont( self.textFont )
            plot._decoratorObjects.append( t )

    # Hook to calculate width and height of the placed objects
    def __calculateSize__( self, plot ):
        nLines = len( plot.titles )
        self.height = nLines * self.textSize + (nLines - 1) * self.lineGap
        if self.labelText:
            self.height += self.labelTextSize + self.lineGap
        self.width = self.defaultWidth

## Special version of the TitleDecorator that also adds the 'ATLAS' label on the plot.
#  The word ATLAS uses bold and italic font 42 as specified in the ATLAS style guide.
class AtlasTitleDecorator( TitleDecorator ):
    ## Default constructor.
    #  @param extraLabel   string that is appended to 'ATLAS' using the label text font
    #  @param position     string defining the relative position of the created objects (see positions, optional, default is "LeftTop") 
    def __init__( self, extraLabel='Internal', position=positions.LeftTop ):
        TitleDecorator.__init__( self, position )
        self.setAtlasLabel(extraLabel)
        self.labelTextSize = 0.05
        self.labelTextFont = 42
        
    def setAtlasLabel(self, value):
        self.labelText = '#it{#bf{ATLAS}}'
        if value:
            self.labelText += ' %s' % value
        
