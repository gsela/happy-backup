## @package happy.variable
#  @brief Classes to describe observables and histogram axes.
#
#  @author Christian Grefe, Bonn University (christian.grefe@cern.ch)
from happy.cut import Cut
from happy.histogramTools import blindHistogram, blindHistogram2D
from happy.stringTools import addParantheses, appendUUID
from array import array
import logging, hashlib

# dictionary of all registered variables, allows look-up by name
VARIABLES = {}

## Helper class to encapsulate two limits.
class Interval( object ):
    
    ## Default constructor.
    #  @param low       lower limit
    #  @param high      upper limit
    def __init__( self, low, high ):
        ## Lower limit
        self.low = low
        ## Upper limit
        self.high = high
    
    ## Apply the interval range to the given axis.
    #  Only works if both, low and high are defined.
    #  @param axis     axis which has attributes updated
    def applyToAxis( self, axis ):
        if self.low and self.high:
            axis.SetRangeUser( self.low, self.high )
        
    def __repr__( self ):
        return 'Interval(%r, %r)' % ( self.low, self.high )
    
    def __eq__( self, other ):
        return isinstance( other, Interval ) and self.low == other.low and self.high == other.high

    def __hash__( self ):
        return hash( ( self.low, self.high ) )
    
    ## Generate an MD5 hash value to identify this interval
    @property
    def md5( self ):
        md5 = hashlib.md5()
        md5.update( str(self.low) )
        md5.update( str(self.high) )
        return md5.hexdigest()

## Class to describe binning of a histogram axis with lower and upper limits
class Binning( Interval ):
    logger = logging.getLogger( __name__ + '.Binning' )
    ## Default constructor
    #  @param nBins      number of bins
    #  @param low        lower edge of the lowest bin
    #  @param high       upper edge of the highest bin
    #  @param binLabels  list of labels for the bins, should match the number of bins (optional)
    def __init__( self, nBins=40, low=None, high=None, binLabels=None ):
        Interval.__init__( self, low, high )
        ## Number of bins
        self.nBins = nBins
        if binLabels and len(binLabels) != nBins:
            self.logger.warning( 'Number of bin labels (%s) does not match number of bins (%d)' % (binLabels, nBins) )
        ## List with bin labels, has to match nBins
        self.binLabels = binLabels
        ## Division of tick marks (see https://root.cern.ch/doc/master/classTGaxis.html#GA11)
        self.nDivisions = 507
        ## Decides if content from the underflow bin should be included in the first bin
        self.includeUnderflowBin = False
        ## Decides if content from the overflow bin should be included in the last bin
        self.includeOverflowBin = False
        ## Decides if histogram entries are divided by bin width (recommended for VariableBinning)
        self.normaliseByBinWidth = False
    
    ## Set the binning of the given axis
    #  @param axis     axis which has attributes updated
    def setupAxis( self, axis ):
        low = self.low if self.low is not None else 0.
        high = self.high if self.high is not None else 0.
        axis.Set( self.nBins, low, high )
    
    ## Apply attributes to the given axis
    #  @param axis     axis which has attributes updated
    def applyToAxis( self, axis ):
        Interval.applyToAxis( self, axis )
        axis.SetNdivisions( self.nDivisions )
        if axis and self.binLabels:
            index = 1
            for label in self.binLabels:
                axis.SetBinLabel( index, str(label) )
                index += 1
    
    def __repr__( self ):
        return 'Binning(%r, %r, %r)' % ( self.nBins, self.low, self.high )
    
    def __eq__( self, other ):
        return isinstance( other, Binning ) and self.nBins == other.nBins and Interval.__eq__( self, other )
    
    def __hash__( self ):
        return hash( ( self.nBins, Interval.__hash__( self ) ) )
    
    ## Generate an MD5 hash value to identify this binning
    @property
    def md5( self ):
        md5 = hashlib.md5()
        md5.update( str(self.nBins) )
        md5.update( str(self.low) )
        md5.update( str(self.high) )
        return md5.hexdigest()
    
    ## Returns an array of the bin boundaries
    @property
    def bins( self ):
        result = array( 'd', (self.nBins+1)*[0] )
        binWidth = (self.high - self.low) / float(self.nBins)
        for iBin in xrange( self.nBins+1 ):
            result[iBin] = self.low + iBin*binWidth
        return result

## Class to describe binning of a histogram axis with varying bin widths
class VariableBinning( Binning ):
    logger = logging.getLogger( __name__ + '.VariableBinning' )
    
    ## Default constructor
    #  @param bins       list of numbers defining the bin boundaries with length of nBins+1
    #  @param binLabels  list of labels for the bins, should match the number of bins (optional)
    def __init__( self, bins=[], binLabels=[] ):
        Binning.__init__( self )
        ## list of bin boundaries with length of nBins+1
        self.bins = bins
        ## list of bin labels
        self.binLabels = binLabels
    
    ## Set the binning of the given axis
    #  @param axis     axis which has attributes updated
    def setupAxis( self, axis ):
        axis.Set( self.nBins, self.bins )
    
    def __repr__( self ):
        return 'VariableBinning(%r, %r, %r)' % ( self.nBins, self.low, self.high )
    
    def __eq__( self, other ):
        return isinstance( other, VariableBinning ) and set( self.bins ) == set( other.bins )
    
    def __hash__( self ):
        return hash( tuple( self.bins ) )
    
    ## Generate an MD5 hash value to identify this binning
    @property
    def md5( self ):
        return hashlib.md5( str( tuple( self.bins ) ) ).hexdigest()
    
    @property
    def bins( self ):
        return self.__bins
    
    @bins.setter
    def bins( self, bins ):
        if not bins:
            self.logger.error( 'Trying to set an empty list of bins' )
            return
        bins.sort()
        self.__bins = array( 'd', bins )
        self.nBins = len(self.__bins)-1
        self.low = self.__bins[0]
        self.high = self.__bins[-1]

def binningFromHistogram(hist):
    xAxis = hist.GetXaxis()
    if xAxis.GetXbins().GetSize():
        bins = []
        binArray = xAxis.GetXbins().GetArray()
        for i in xrange(xAxis.GetXbins().GetSize()):
            bins.append(binArray[i])
        for iBin in xrange(2,xAxis.GetXbins().GetSize()):
            if bins[iBin]-bins[iBin-1] != bins[iBin-1]-bins[iBin-2]:
                return VariableBinning(bins)
        return Binning(xAxis.GetXbins().GetSize()-1, bins[0], bins[-1])
    else:
        return Binning(xAxis.GetNbins(), xAxis.GetXmin(), xAxis.GetXmax())

## Class to describe physics observables.
#  Provides methods to create corresponding histograms and even fill them directly from
#  a given TTree. Simple arithmetic operations between two variables are defined and
#  will result in the correct combined Variable including updated unit. Valid operators
#  are "+", "-", "*" and "/".
#  @note Two variables are considered equal if they have identical values for command,
#  binning and defaultCut. Name, title and unit are ignored as they don't change the
#  meaning of the variable and will result in identical histograms.
class Variable( object ):
    logger = logging.getLogger( __name__ + '.Variable' )
    ## Default constructor.
    #  @param name        name of the variable
    #  @param command     command used in TTree::Draw (optional, default uses name)
    #  @param title       title used for example in axis lables (optional, default uses name)
    #  @param unit        title of the unit (optional, default is an empty string)
    #  @param binning     Binning object (optional, default uses 40 bins with automatic range)
    #  @param defaultCut  Cut that will be applied in addition whenever this variable is plotted (optional)
    def __init__( self, name, command=None, title=None, unit='', binning=None, defaultCut=None ):
        ## Name of the variable (used in file names, etc.)
        self.name = name
        ## Command used in TTree::Draw (can be a formula)
        self.command = command if command is not None else name
        ## Title of the variable (used in axis labels, etc.)
        self.title = title if title is not None else name
        ## Title of the unit (used in axis labels)
        self.unit = unit
        ## Binning applied to corresponding histogram axes
        self.binning = binning if binning else Binning()
        ## Cut that will be applied in addition whenever this variable is plotted
        self.defaultCut = defaultCut if defaultCut else Cut()
        ## Sets a corresponding truth matched variable.
        #  Allows for automatic generation of resolution variables, etc.
        self.matchedVariable = None
        ## Defines the periodicity of this variable using a TTree::Draw expression.
        #  Used when generating resolution variables, etc.
        self.periodicity = None
        ## Define if blinding is applied to histograms of this variable
        self.isBlinded = True
        ## Define if the axis title should be centered or not
        self.centerTitle = False
        # internal storage of blinded regions
        self._blindingScheme = {}
    
    ## Constructor from an XML element.
    #  @code
    #  <Variable name="VariableName" command="DrawCommand" title="AxisTitle" unit="Unit" nBins="nBins" low="min" up="max">
    #    <Cut>DefaultCut</Cut>
    #  </Variable>
    #  @endcode
    #  @param element    XML element
    #  @return the Variable object
    @classmethod
    def fromXML( cls, element ):
        attributes = element.attrib
        name = attributes[ 'name' ]
        variable = cls( name )
        if attributes.has_key( 'command' ):
            variable.treeName = attributes['command']
        if attributes.has_key( 'title' ):
            variable.title = attributes['title']
        if attributes.has_key( 'unit' ):
            variable.unit = attributes['unit']
        if attributes.has_key( 'nBins' ):
            variable.binning.nBins = int(attributes['nBins'])
        if attributes.has_key( 'low' ):
            variable.binning.low = float(attributes['low'])
        if attributes.has_key( 'up' ):
            variable.binning.high = float(attributes['up'])
        for cutElement in element.findall( 'Cut' ):
            variable.defaultCut += Cut.fromXML( cutElement )
        return variable
    
    def __repr__( self ):
        return 'Variable(%r, %r, %r, %r, %r, %r)' % (self.name, self.command, self.title, self.unit, self.binning, self.defaultCut)
    
    def __str__( self ):
        return self.command
    
    def __eq__( self, other ):
        return isinstance( other, Variable ) and self.command == other.command and self.binning == other.binning and self.defaultCut == other.defaultCut
    
    def __hash__( self ):
        return hash( ( self.command, hash( self.binning ), hash( self.defaultCut ) ) )
    
    ## Generate an MD5 hash value to identify this variable.
    #  The command, binning and defaultCut members are used.
    #  Name, title and unit are ignored.
    @property
    def md5( self ):
        md5 = hashlib.md5()
        md5.update( self.command )
        md5.update( str( self.binning.md5 ) )
        md5.update( self.defaultCut.cut )
        return md5.hexdigest()
    
    @property
    def name( self ):
        return self.__name
    
    @name.setter
    def name( self, name ):
        # remove the old reference
        if hasattr( self, '__name' ) and VARIABLES.has_key( self.__name ):
            del VARIABLES[self.__name]
        self.__name = name
        # register variable
        if not VARIABLES.has_key( self.__name ):
            VARIABLES[self.__name] = self

    ## Generates a new Variable corresponding to "self - matched", if matchedVariable is defined.
    @property
    def deltaVar( self ):
        if not self.matchedVariable:
            self.logger.error( 'deltaVar(): no matched variable available for %r' % self )
            return
        deltaVar = self - self.matchedVariable
        if self.periodicity:
            deltaVar.command = 'min(abs(%s), abs(abs(%s)-%s))*sign(%s)' % (deltaVar.command, deltaVar.command, self.periodicity, deltaVar.command)
        deltaVar.name = 'delta_' + self.name
        return deltaVar

    ## Generates a new Variable corresponding to "(this - matched) / matched", if matchedVariable is defined.
    @property
    def deltaRelativeVar( self ):
        if not self.matchedVariable:
            self.logger.error( 'deltaRelativeVar(): no matched variable available for %r' % self )
            return
        deltaVar = (self - self.matchedVariable) / self.matchedVariable
        deltaVar.name = 'delta-relative_' + self.name
        return deltaVar

    ## The corresponding RooRealVar to be used in RooFit.
    @property
    def rooFitVar( self ):
        if not self.__rooFitVar:
            from ROOT import RooRealVar
            self.__rooFitVar = RooRealVar( self.name, self.title, self.low, self.high, self.unit )
            self.__rooFitVar.setBins( self.nBins )
        return self.__rooFitVar

    ## Full axis label including unit.
    @property
    def axisLabel( self ):
        label = self.title
        if self.unit:
            if '\\' in label:
                label += '\\text{ [%s]}' % self.unit
            else:
                label += ' [%s]' % self.unit
        return label

    ## Apply attributes to the given axis
    #  @param axis     axis which has attributes updated
    def applyToAxis( self, axis ):
        self.binning.applyToAxis( axis )
        axis.SetTitle( self.axisLabel )
        axis.CenterTitle( self.centerTitle )

    ## Adds a blinded region for this variable defined by a Cut, a lower and an upper value.
    #  @note: The blinding is only applied if isBlinded is also set to True
    #  @param cut      cut defining the region in which the blinding is applied
    #  @param low      lower edge of the blinded region
    #  @param high     upper edge of the blinded region
    def blindRegion( self, cut, low, high ):
        self._blindingScheme[ cut ] = ( low, high )

    ## Applies the blinding scheme defined for this variable to the given histogram.
    #  Only modifies the histogram if isBlinded is set to True.
    #  This can not be done automatically in createHistogram since it should only apply to data.
    #  @param cut          cut defining the region contained in the histogram
    #  @param histogram    TH1 object to be blinded
    #  @return the blinded histogram
    def applyBlinding( self, cut, histogram ):
        if self.isBlinded and self._blindingScheme.has_key( cut ):
            low, high = self._blindingScheme[ cut ]
            histogram = blindHistogram( histogram, low, high )
        return histogram
    
    ## Applies the blinding scheme defined for this variable to the given histogram.
    #  Only modifies the histogram if isBlinded is set to True for this Variable and/or
    #  the y Variable. Blinding for x and y bins are treated seperately.
    #  This can not be done automatically in createHistogram since it should only apply to data.
    #  @param cut          cut defining the region contained in the histogram
    #  @param yVar         Variable object defining the y axis
    #  @param histogram    TH1 object to be blinded
    #  @return the blinded histogram
    def applyBlinding2D( self, cut, yVar, histogram ):
        lowX, highX, lowY, highY = (0,0,0,0)
        if self.isBlinded and self._blindingScheme.has_key( cut ):
            lowX, highX = self._blindingScheme[ cut ]
        if yVar.isBlinded and yVar._blindingScheme.has_key( cut ):
            lowY, highY = self._blindingScheme[ cut ]
        histogram = blindHistogram2D( histogram, lowX, highX, lowY, highY )
        return histogram
    
    ## Create an empty histogram for this variable.
    #  @param title      title of the new histogram (optional, default uses title of the variable)
    #  @param profile    decide if a TProfile should be created instead
    #  @return empty TH1 or TProfile
    def createHistogram( self, title=None, profile=False ):
        title = title if title is not None else self.title
        name = appendUUID( 'h%s' % ( self.name.replace(' ', '').replace('(', '').replace(')','') ) )
        if profile:
            from ROOT import TProfile
            h = TProfile( name, title, self.binning.nBins, 0., 1. )
        else:
            from ROOT import TH1F
            h = TH1F( name, title, self.binning.nBins, 0., 1. )
        if not h.GetSumw2N():
            h.Sumw2()
        self.binning.setupAxis( h.GetXaxis() )
        self.applyToAxis( h.GetXaxis() )
        return h

    ## Create an empty 2D histogram for this variable.
    #  @param yVar       Variable object defining the y axis
    #  @param title      title of the new histogram (optional, default uses "x.title vs y.title")
    #  @param profile    decide if a TProfile2D should be created instead
    #  @return empty TH2 or TProfile2D
    def create2DHistogram( self, yVar, title=None, profile=False ):
        title = title if title is not None else '%s vs %s' % (yVar.title, self.title)
        name = appendUUID( 'h_%s_vs_%s' % ( yVar.name.replace(' ', '').replace('(', '').replace(')',''), self.name.replace(' ', '').replace('(', '').replace(')','') ) )
        if profile:
            from ROOT import TProfile2D
            h = TProfile2D( name, title, self.binning.nBins, 0., 1., yVar.binning.nBins, 0., 1. )
        else:
            from ROOT import TH2D
            h = TH2D( name, title, self.binning.nBins, 0., 1., yVar.binning.nBins, 0., 1. )
        if not h.GetSumw2N():
            h.Sumw2()
        self.binning.setupAxis( h.GetXaxis() )
        self.applyToAxis( h.GetXaxis() )
        yVar.binning.setupAxis( h.GetYaxis() )
        yVar.applyToAxis( h.GetYaxis() )
        return h

    ## Create a histogram of this variable from a TTree using TTree::Draw.
    #  @param tree             TTree object used to create the histogram
    #  @param title            the histogram title
    #  @param weightExpression Cut object describing the weight expression
    #  @param drawOption       draw option used
    #  @param style            Style object applied to the histogram (optional)
    #  @param nEntries         number of entries to process in the tree (default: all entries)
    #  @param firstEntry       first entry to process in the tree (default: 0)
    #  @return filled TH1
    def createHistogramFromTree( self, tree, title='', weightExpression=None, drawOption='', style=None, nEntries=-1, firstEntry=0 ):
        if not tree:
            return
        if not nEntries or nEntries < 0:
            from ROOT import TTree
            nEntries = TTree.kMaxEntries
        cut = weightExpression if weightExpression else Cut()
        cut *= self.defaultCut
        opt = drawOption + 'goff'
        # create an empty histogram
        h = self.createHistogram( title )
        # no range set, need to determine from values
        if self.binning.low is None or self.binning.high is None:
            self.logger.debug( 'createHistogramFromTree(): missing range from binning - determining range automatically.' )
            tree.Draw( '%s >> temp(%d)' % ( self.command, 1000 ), cut.cut, opt )
            hTemp = tree.GetHistogram()
            if hTemp:
                low = max(hTemp.GetXaxis().GetXmin(), hTemp.GetMean()-3*hTemp.GetStdDev()) if self.binning.low is None else self.binning.low
                up = min(hTemp.GetXaxis().GetXmax(), hTemp.GetMean()+3*hTemp.GetStdDev()) if self.binning.high is None else self.binning.high
                # reset axis with the new limits
                h.GetXaxis().Set( self.binning.nBins, low, up )
            else:
                self.logger.error( 'createHistogramFromTree(): no histogram created from TTree::Draw( "%s", "%s", "%s" )' % (self.command, cut.cut, drawOption) )
        self.logger.debug( 'createHistogramFromTree(): calling TTree::Draw( "%s >> %s", "%s", "%s" )' % (self.command, h.GetName(), cut.cut, drawOption) )
        tree.Draw( '%s >> %s' % (self.command, h.GetName()), cut.cut, opt, nEntries, firstEntry )
        if h:
            self.logger.debug( 'createHistogramFromTree(): created histogram with %d entries and an integral of %g' % (h.GetEntries(), h.Integral()) )
            if style:
                style.apply( h )
        else:
            self.logger.error( 'createHistogramFromTree(): no histogram created from TTree::Draw( "%s", "%s", "%s" )' % (self.command, cut.cut, drawOption) )
        return h
    
    ## Helper method to create a 2D histogram from a TTree using TTree:Draw.
    #  @param tree             TTree object used to create the histogram
    #  @param yVar             Variable object defining the y-axis draw command
    #  @param title            histogram title
    #  @param weightExpression Cut object describing the weight expression (optional)
    #  @param drawOption       draw option used
    #  @param style            Style object applied to the histogram (optional)
    #  @return filled TH2 or TProfile
    def create2DHistogramFromTree( self, tree, yVar, title='', weightExpression=None, drawOption='', style=None ):
        if not tree:
            return
        cut = weightExpression if weightExpression else Cut()
        cut *= self.defaultCut * yVar.defaultCut
        opt = drawOption + 'goff'
        if 'prof' in opt.lower():
            h = self.createHistogram( title, True )
        else:
            h = self.create2DHistogram( yVar, title )
        # no range set, need to determine from values
        if self.binning.low is None or self.binning.high is None or yVar.binning.low is None or yVar.binning.high is None:
            self.logger.debug( 'createHistogramFromTree(): missing range from binning - determining range automatically.' )
            tree.Draw( '%s : %s >> temp' % ( yVar.command, self.command ), cut.cut, opt )
            hTemp = tree.GetHistogram()
            if hTemp:
                xLow = max(hTemp.GetXaxis().GetXmin(), hTemp.GetMean(1)-3*hTemp.GetStdDev(1)) if self.binning.low is None else self.binning.low
                xUp = min(hTemp.GetXaxis().GetXmax(), hTemp.GetMean(1)+3*hTemp.GetStdDev(1)) if self.binning.high is None else self.binning.high
                yLow = max(hTemp.GetYaxis().GetXmin(), hTemp.GetMean(2)-3*hTemp.GetStdDev(2)) if yVar.binning.low is None else yVar.binning.low
                yUp = min(hTemp.GetYaxis().GetXmax(), hTemp.GetMean(2)+3*hTemp.GetStdDev(2)) if yVar.binning.high is None else yVar.binning.high
                # reset axis with the new limits
                h.GetXaxis().Set( self.binning.nBins, xLow, xUp )
                h.GetYaxis().Set( yVar.binning.nBins, yLow, yUp )
            else:
                self.logger.debug( 'create2DHistogramFromTree(): no histogram created from TTree::Draw( "%s : %s", "%s", "%s" )' % (yVar.command, self.command, cut.cut, drawOption ) )
        self.logger.debug( 'create2DHistogramFromTree(): calling TTree::Draw( "%s : %s", "%s", "%s" )' % (yVar.command, self.command, cut.cut, drawOption ) )
        tree.Draw( '%s : %s >> %s' % (yVar.command, self.command, h.GetName()), cut.cut, opt )
        if h:
            self.logger.debug( 'create2DHistogramFromTree(): created histogram with %d entries and an integral of %g' % (h.GetEntries(), h.Integral()) )
            if style:
                style.apply( h )
        else:
            self.logger.debug( 'create2DHistogramFromTree(): no histogram created from TTree::Draw( "%s : %s", "%s", "%s" )' % (yVar.command, self.command, cut.cut, drawOption ) )
        return h
    
    # implement -self operator
    def __neg__( self ):
        command = '-' + addParantheses( self.command, '+-' )
        title = '-' +  addParantheses( self.title, '+-' )
        return Variable( 'inv_'+self.name, command, title, self.unit, self.binning, self.defaultCut )
    
    # implement "+" operator
    def __add__( self, other ):
        command = '%s+%s' % (addParantheses( self.command, '*/' ), addParantheses( other.command, '*/' ) )
        title = '%s + %s' % (addParantheses( self.title, '*/' ), addParantheses( other.title, '*/' ) )
        if self.unit is not other.unit:
            self.logger.warning( '__add__(): combining variables %r and %r with different units.' % (self, other) )
        return Variable( '%s_PLUS_%s' % (self.name, other.name), command, title, self.unit, self.binning, self.defaultCut+other.defaultCut )
    
    # implement "-" operator
    def __sub__( self, other ):
        command = '%s-%s' % (addParantheses( self.command, '*/' ), addParantheses( other.command, '*/' ) )
        title = '%s - %s' % (addParantheses( self.title, '*/' ), addParantheses( other.title, '*/' ) )
        if self.unit is not other.unit:
            self.logger.warning( '__add__(): combining variables %r and %r with different units.' % (self, other) )
        return Variable( '%s_MINUS_%s' % (self.name, other.name), command, title, self.unit, self.binning, self.defaultCut+other.defaultCut )
    
    # implement "*" operator
    def __mul__( self, other ):
        command = '%s*%s' % (addParantheses( self.command, '+-' ), addParantheses( other.command, '+-' ) )
        title = '%s * %s' % (addParantheses( self.title, '+-' ), addParantheses( other.title, '+-' ) )
        if self.unit == other.unit:
            unit = self.unit + '^{2}'
        else:
            unit = self.unit+other.unit
        return Variable( '%s_TIMES_%s' % (self.name, other.name), command, title, unit, self.binning, self.defaultCut+other.defaultCut )
    
    # implement "/" operator
    def __div__( self, other ):
        command = '%s/%s' % (addParantheses( self.command, '+-' ), addParantheses( other.command, '+-' ) )
        title = '%s / %s' % (addParantheses( self.title, '+-' ), addParantheses( other.title, '+-' ) )
        if self.unit == other.unit:
            unit = ''
        else:
            unit = self.unit+other.unit+'^{-1}'
        return Variable( '%s_DIV_%s' % (self.name, other.name), command, title, unit, self.binning, self.defaultCut+other.defaultCut )

## Class to represent a combination of different variables.
#  When used to create a histogram, the resulting histogram is the sum of
#  all individual histograms. This can be used for example to draw a combined
#  histogram for multiple branches at the same time, i.e. leading and sub-leading
#  jet pT into a single jet pT distribution.
class MultiVariable( Variable ):
    logger = logging.getLogger( __name__ + '.CombinedVariable' )
    
    ## Default constructor.
    #  @param name        name of the variable
    #  @param title       title used for example in axis lables (optional, default uses name)
    #  @param unit        title of the unit (optional, default is an empty string)
    #  @param binning     Binning object
    #  @param defaultCut  Cut that will be applied in addition whenever this variable is plotted (optional)
    #  @param variables   List of Variable object that will be plotted at the same time
    def __init__( self, name, title=None, unit='', binning=Binning(), defaultCut=Cut(), variables=[] ):
        Variable.__init__( self, name, None, title, unit, binning, defaultCut )
        self.variables = variables
    
    def createHistogramFromTree( self, tree, title='', cut=None, weight=None, drawOption='', style=None ):
        cut = cut if cut else Cut()
        cut += self.defaultCut
        hist = self.createHistogram( title )
        for variable in self.variables:
            h = variable.createHistogramFromTree( tree, title, cut, weight, drawOption )
            if h:
                hist.Add( h )
        if hist and style:
            style.apply( hist )
        return hist

    def create2DHistogramFromTree( self, tree, yVar, title='', cut=None, weight=None, drawOption='', style=None ):
        cut = cut if cut else Cut()
        cut += self.defaultCut
        if 'prof' in drawOption.lower():
            hist = self.createHistogram( title, True )
        else:
            hist.create2DHistogram( yVar, title )
        for variable in self.variables:
            h = variable.create2DHistogramFromTree( tree, yVar, title, cut, weight, drawOption )
            if h:
                hist.Add( h )
        if hist and style:
            style.apply( hist )
        return hist

## Helper method to create a cutflow variable from a list of Cut objects
#  Each
#  @param name    name of the variable
#  @param cuts    the list of cuts that define the x-axis
#  @return the variable
def createCutFlowVariable( name='cutflow', cuts=[] ):
    binLables = []
    for cut in cuts:
        binLables.append( cut.title )
    binning = Binning( len(cuts), 0, len(cuts) )
    binning.binLabels = binLables
    return Variable( name, '', '', '', binning )


# some standard variable definitions for axis labels, lower limit 0, upper limit free
var_AU         = Variable( 'A.U.', binning=Binning(40) )
var_Events     = Variable( 'Events', binning=Binning(40) )
var_Entries    = Variable( 'Entries', binning=Binning(40) )
var_Ratio      = Variable( 'Ratio', binning=Binning(40) )
var_Normalized = Variable( 'Normalized Entries', binning=Binning(40) )
var_Yield      = Variable( 'yield', '0', '', '', Binning(1, 0, 1, ['']) )

if __name__ == '__main__':
    # define an interval
    testInterval = Interval( -10., 15. )
    print testInterval
    
    # define a binning
    testBinning = Binning( 25, 0, 25. )
    print testBinning
    print testBinning.bins
    
    # define a binning with custom bin labels
    testBinningWithLabels = Binning( 5, 0, 5, ['1p0n', '1p1n', '1pXn', '3p0n', '3pXn'] )
    print testBinningWithLabels
    
    # define a binning with variable bin widths
    testVariableBinning = VariableBinning( [0,0.3,1,3,4,7] )
    print testVariableBinning
    
    # define some variables
    tauEnergy = Variable( 'tau_energy', 'tau_energy', 'E(#tau)', 'GeV', testBinning )
    tauPt     = Variable( 'tau_pt', 'tau_pt', 'p_{T}(#tau)', 'GeV', testBinning )
    print tauEnergy
    print tauPt
    
    # combine variables, note the treatment of the unit
    print tauEnergy - tauPt
    print tauEnergy * tauPt
    print tauEnergy / tauPt
    
    # define a matched variable and get standard resolution observables
    tauPt.matchedVariable = Variable( 'tau_matched_pt', 'tau_matched_pt', 'p^{true}_{T}(#tau)', 'GeV', testBinning )
    print tauPt.deltaVar
    print tauPt.deltaRelativeVar
    
    # example plot
    #from BasicPlot import BasicPlot
    # create an empty histogram from the variable object
    #h = tauEnergy.createHistogram()
    #import ROOT
    #r = ROOT.TRandom3()
    #for x in xrange(10000):
    #    h.Fill( r.Gaus( 2, 5 ) )
    #p = BasicPlot( 'testVariableBinning' )
    #p.addHistogram( h )
    #p.draw()
    
    raw_input( 'Continue?' )
