## @package happy.crossSectionDB
#  @brief Classes to provide access to cross section information
#
#  @author Christian Grefe, Bonn University (christian.grefe@cern.ch)
import csv, logging, os

SusyCrossSectionFile = os.path.join( os.environ.get('HAPPyDIR'), 'data', 'SUSY_XS', 'susy_crosssections_13TeV.txt' )
PmgCrossSectionFile = os.path.join( os.environ.get('HAPPyDIR'), 'data', 'PMG_XS', 'CrossSectionData.txt' )

## Class to encapsulate cross section information
class CrossSectionDBEntry( object ):
    
    ## Default constructor
    #  @param dsid            dataset ID (int)
    #  @param name            name of the dataset (optional, default is using dsid)
    #  @param crossSection    cross section in pb (optional, default is 0)
    #  @param branchingRatio  branching ratio of this sample (optional, default is 1)
    #  @param efficiency      filter efficiency (optional, default is 1)
    #  @param kFactor         correction factor to cross section (optional, default is 1)
    #  @param uncertainty     relative uncertainty on cross section (optional, default is 0)
    def __init__( self, dsid, name=None, crossSection=0., branchingRatio=1., efficiency=1., kFactor=1., uncertainty=0. ):
        self.dsid = dsid
        self.name = name if name is not None else str(dsid)
        self.crossSection = crossSection
        self.branchingRatio = branchingRatio
        self.efficiency = efficiency
        self.kFactor = kFactor
        self.uncertainty = uncertainty
        self.comment = ''
    
    ## Constructor from a dictionary
    #  The dictionary has to contain the keys "dsid", "name", "crossSection", "efficiency",
    #  and "kFactor". In addition, "branchingRatio" and "uncertainty" are optional keys.
    #  @param d       input dictionary
    @classmethod
    def fromDict( cls, d ):
        entry = cls( int(d['dsid']), d['name'], float(d['crossSection']), efficiency=float(d['efficiency']), kFactor=float(d['kFactor']) )
        if d.has_key( 'branchingRatio' ):
            entry.uncertainty = float( d['branchingRatio'] )
        if d.has_key( 'uncertainty' ):
            entry.uncertainty = float( d['uncertainty'] )
        return entry
    
    ## Calculates the effective cross section in pb
    #  branching ratio, efficiency and higher order corrections (k-factor)
    #  are all taken into account.
    #  @return float
    @property
    def effectiveCrossSection( self ):
        return self.crossSection * self.branchingRatio * self.efficiency * self.kFactor

    def __repr__( self ):
        return 'CrossSectionDBEntry(%r, %r, %r, %r, %r, %r, %r)' % (self.dsid, self.name, self.crossSection, self.branchingRatio, self.efficiency, self.kFactor, self.uncertainty)

    def __str__( self ):
        return '%s: %s pb' % (self.dsid, self.effectiveCrossSection)

## Dictionary class handling cross section book keeping.
#  Entries are stored based on their DSID, i.e. the DSID has to be unique.
#  @note Use the instance already provided in this module which is used
#  in several places throughout the framework to automatically retrieve
#  the cross sections.
#  @code
# from happy.analysis.CrossSectionDB import DB, PmgCrossSectionFile
# DB.readTextFile( PmgCrossSectionFile )
# print DB[341001]
#  @endcode
class CrossSectionDB( dict ):
    logger = logging.getLogger( __name__ + '.CrossSectionDB' )
    columnsPMG = ['dsid', 'name', 'crossSection', 'branchingRatio', 'efficiency', 'hoCrossSection', 'kFactor', 'hoSampleCrossSection']
    columnsSUSY = ['dsid', 'name', 'crossSection', 'kFactor', 'efficiency', 'uncertainty']
    columnsPMGnew = ['dsid', 'name', 'crossSection', 'efficiency', 'kFactor', 'uncertainty', 'uncertainty', 'generator']

    ## Default constructor
    def __init__( self ):
        pass
    
    ## Constructor from an XML element.
    #  @code
    #  <CrossSectionDB>
    #    <File> File1 </File>
    #    <File> File2 </File>
    #  </Dataset>
    #  @endcode
    #  @param element    the XML element
    #  @return CrossSectionDB object
    @classmethod
    def fromXML( cls, element ):
        for fileElement in element.findall( 'File' ):
            crossSectionDB.readTextFile( fileElement.text.strip() )
        return crossSectionDB
    
    ## Adds a new entry to the database.
    #  @param dsid            dataset ID (int)
    #  @param name            name of the dataset (optional, default is using dsid)
    #  @param crossSection    cross section in pb
    #  @param branchingRatio  branching ratio of this sample (optional, default is 1)
    #  @param efficiency      filter efficiency (optional, default is 1)
    #  @param kFactor         correction factor to cross section (optional, default is 1)
    #  @param uncertainty     relative uncertainty on cross section (optional, default is 0)
    def add( self, dsid, name, crossSection, branchingRatio=1., efficiency=1., kFactor=1., uncertainty=0. ):
        self[dsid] = CrossSectionDBEntry( dsid, name, crossSection, branchingRatio, efficiency, kFactor, uncertainty )
    
    ## Read an input text file and add its content to this DB.
    #  Uses the CrossSectionDBEntry.fromDict, where columns are assumed to be tab-seperated.
    #  The different conventions used in SUSYTools and in the PMG database are supported.
    #
    #  The column conventions are:
    #    - SUSY: "dsid" "name" "crossSection" "kFactor" "efficiency" "uncertainty"
    #    - PMG: "dsid" "name" "crossSection" "branchingRatio" "efficiency" "hoCrossSection" "kFactor" "hoSampleCrossSection"
    #
    #  @param fileName      input file name
    def readTextFile( self, fileName, version='old'):
        self.logger.debug( 'readTextFile(): reading cross-sections from "%r"' % ( fileName ) )
        f = open( fileName, 'r' )
        import string
        # determine the format of the input file
        if version=='new':
            columnNames = self.columnsPMGnew
            self.logger.debug( 'readTextFile(): using new PMG XS column scheme' )
        elif 'id/I:name/C:xsec/F:kfac/F:eff/F:relunc/F' in f.readline():
            columnNames = self.columnsSUSY
            self.logger.debug( 'readTextFile(): using SUSY XS column scheme' )
        else:
            columnNames = self.columnsPMG
            self.logger.debug( 'readTextFile(): using PMG XS column scheme' )
        reader = csv.DictReader( (string.join(row.split(), ' ') for row in f if (not row.startswith( '#' ) and not row.startswith( 'id' ))), columnNames, delimiter=' ' )
        n = 0
        for row in reader:
            if not row['dsid']:
                continue
            try:
                entry = CrossSectionDBEntry.fromDict( row )
            except (ValueError, TypeError):
                self.logger.warning( 'readTextFile(): unable to convert "%s"' % row  )
                continue
            self.logger.debug( 'readTextFile(): found %s' % ( entry ) )
            self[entry.dsid] = entry
            if columnNames == self.columnsPMG:
                entry.name = entry.name.split( '.' )[2]
            n += 1
        self.logger.info( 'readTextFile(): successfully read %d cross-section entries from "%s"' % ( n, fileName ) )


## Single instance of the cross section database
crossSectionDB = CrossSectionDB()
         
if __name__ == "__main__":
    #DB.readTextFile( SusyCrossSectionFile )
    crossSectionDB.readTextFile( PmgCrossSectionFile )
    
    entry = crossSectionDB[341157]
    print entry, '[%s]' % entry.name    
