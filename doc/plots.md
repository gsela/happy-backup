Making plots
====

Plots are drawn using the `Plot` class or some of its derived classes. A `Plot` accepts all kind of objects that derive from `TH1`, `TGraph` or `TF1` and takes care of drawing objects in the right order with the desired draw options. Stacking of histograms is also supported. A legend is generated automatically using the titles of the drwan objects. See [examples/plots.py](../examples/plots.py) for the corresponding examples.

Simple plots
------
We start by creating some ROOT objects to plot:
```python
import ROOT
from happy.plot import Plot
h1 = ROOT.TH1D('h1', 'First histogram', 40, -4, 4)
h1.FillRandom('gaus', 2000)
h1.SetLineColor(ROOT.kRed)
h1.SetMarkerColor(ROOT.kRed)

h2 = ROOT.TH1D('h2', 'Second histogram', 40, -4, 4)
h2.FillRandom('gaus', 1000)
h2.SetMarkerColor(ROOT.kBlue)
h2.SetLineColor(ROOT.kBlue)
h2.SetLineWidth( 3 )
# need to set fill color explicitely to 0 to ensure no fill for stacked histograms
h2.SetFillStyle( 0 )

h3 = ROOT.TH1D('h3', 'Test data', 40, -4, 4)
h3.FillRandom('gaus', 3000)
h3.SetLineColor(ROOT.kBlack)

f1 = ROOT.TF1('f1', 'gaus', -4, 4)
f1.SetLineColor(ROOT.kGreen+2)

f2 = ROOT.TF2('f2','sin(x)*sin(y)/(x*y)+1',-4,4,-4,4);
h2D = ROOT.TH2D('h2D', '2D histogram', 20, -4, 4, 20, -4, 4)
h2D.FillRandom('f2', 50000)
```
Next, we add some of them to a `Plot` object and draw it:
```python
plot = Plot('My first plot')
# adding a histogram with a specific draw option
plot.addHistogram(h1, 'E0')
# default draw option for histograms is 'HIST'
plot.addHistogram(h2)
plot.draw()
plot.saveAs('plots01.png')
```
![doc/figures/plots01.png](doc/figures/plots01.png)

See [THistPainter](https://root.cern/doc/master/classTHistPainter.html) and [TGraphPainter](https://root.cern.ch/doc/master/classTGraphPainter.html) for description of the draw options. Objects are typically drawn in the order they have been added. The legend is generated automatically from the object titles in the same order from bottom to top. The y-axis range is determined automatically if not specified by the `Variable` of the y-axis (see below).

A `Plot` can also be drawn with all histograms normalised to unity. Any object can also be linked to a function which will be fitted during the draw step:
```python
plot = Plot('My second plot')
# an internal copy can be generated which is useful if the plot modifies the histogram, e.g. when normalising the histograms. The function returns the copied object.
h1copy = plot.addHistogram(h1, copy=True)
plot.addHistogram(h2, 'E0', copy=True)
# add a fit function to one of the histograms
plot.addFitFunction(h1copy, f1, legendTitle='Fit to first histogram')
# normalise all histograms to unity
plot.normalized = True
# the fit is performed in the draw step, i.e. after the normalisation step
plot.draw()
plot.saveAs('plots02.png')
```
![doc/figures/plots02.png](doc/figures/plots02.png)

Histograms can be drawn stacked if specified:
```python
plot = Plot('My third plot')
plot.addHistogram(h1, stacked=True)
plot.addHistogram(h3, 'E')
plot.addHistogram(h2, stacked=True)
plot.logY = True
plot.draw()
plot.saveAs('plots03.png')
```
![doc/figures/plots03.png](doc/figures/plots03.png)

There is only a single `THStack` object which is always draw first, i.e. all non-stacked histograms appear on top. The legend on the other hand is generated in the exact order the objects were added. If no fill colour was specified for a stacked histogram, the line colour is used instead. On the other hand, if the fill colour is explicitely set to 0, only the line will be drawn for that histogram.

Defining variables: labels, binning and axis ranges
------
`Variable` objects are used to define axis labels but in fact are much more than that. They also contain a `Binning` object which defines a histogram binning as well as axis ranges.
```python
from happy.variable import Variable, Binning, binningFromHistogram
varX = Variable('Invariant mass', unit='GeV', binning=Binning(40, -4, 4))
# reading binning from an existing histogram
varX.binning = binningFromHistogram(h1)
# changing the limits
varX.binning.low = -2
varX.binning.high = 3
varY = Variable('Events')
varY.binning.high = 400
plot.variableX = varX
plot.variableY = varY
plot.draw()
plot.saveAs('plots04.png')
```
![doc/figures/plots04.png](doc/figures/plots04.png)

While the number of bins of the `Binning` object are not used when drawing a `Plot` they can be used to generate empty histograms from a `Variable` object. A `Binning` object can also take a list of strings to define its bin labels.
```python
# create a Binning with bin labels
varX = Variable('Option', binning=Binning(4, -1, 3, ['A','B','C','D']))
# create an empty histogram from a Variable object
h4 = varX.createHistogram('Selected option')
h4.FillRandom('gaus', 100)
plot = Plot('My fourth plot')
plot.addHistogram(h4)
plot.draw()
plot.saveAs('plots05.png')
```
![doc/figures/plots05.png](doc/figures/plots05.png)

Histograms with varying bin widths can be generated using the `VariableBinning` class instead:
```python
from happy.variable import VariableBinning
varX.binning = VariableBinning([-2,-1,-0.5,-0.2,0,0.2,0.5,1,2])
h5 = varX.createHistogram('Variable bin size')
h5.FillRandom('gaus', 1000)
plot = Plot('My fifth plot')
plot.addHistogram(h5)
plot.draw()
plot.saveAs('plots06.png')
```
![doc/figures/plots06.png](doc/figures/plots06.png)

Changing the legend
-----
A `Plot` can contain different `PlotDecorator` objects that are called in the `draw` command and which can modify the plot, e.g. draw additional objects. One `PlotDecorator` that is added by default is a `LegendDecorator` which generates a legend entry for each drawn object using the title and the colour attributes as well as the draw option. The `LegendDecorator` defines the text font and its size as well several other attributes that define the layout of the legend.
```python
from happy.plot import LegendElement
from happy.plotDecorator import positions
plot = Plot('My sixth plot', varX)
plot.addHistogram(h1, stacked=True)
plot.addHistogram(h3, 'E')
# add an additional LegendElement
plot.legendElements.append(LegendElement(None, 'Placeholder'))
plot.addHistogram(h2, stacked=True)
# add a label to the legend
plot.legendDecorator.labelText = 'My Label'
plot.legendDecorator.labelTextSize = 0.03
# modify the position and the layout of the legend
plot.legendDecorator.position = positions.LeftTop
plot.legendDecorator.textSize = 0.03
plot.legendDecorator.textFont = 52
plot.legendDecorator.maxEntriesPerColumn = 2
plot.draw()
plot.saveAs('plots07.png')
```
![doc/figures/plots07.png](doc/figures/plots07.png)

An object with an empty title will not be added to the legend. Additional `LegendElement` objects can be added by hand. They are drawn in the same order as any other object that is added to the legend.
Instead of modifying the `plot.legendDecorator` of individual plots one can also modify `Plot.defaultLegendDecorator`  to change the legend of all `Plot` objects created afterwards.
If no legend should be drawn simply set `plot.drawLegend = False`.

Adding titles and other text
-----
Another `PlotDecorator` that is enabled by default is the `TitleDecorator` which adds one line of text for each item in the list of titles. The `AtlasTitleDecorator` is a special version which also adds the ATLAS label above the title lines. Arbitrary text can be added by adding `TextElement` objects to a `Plot`.
```python
from happy.plotDecorator import AtlasTitleDecorator
from happy.plot import TextElement
# need to remove the old title decorator from this plot as it was drawn before
plot.decorators = []
# define some title lines
plot.titles.append('This is some title text')
plot.titles.append('... and this is another line of title text')
# add the ATLAS label
plot.titleDecorator = AtlasTitleDecorator('Preliminary')
# change the position and the layout of the title
plot.titleDecorator.position = positions.LeftBottom
plot.titleDecorator.textSize = 0.03
# add an additional TextElement
plot.textElements.append(TextElement('Great plot!', alignment=22, color=0, x=0.5, y=0.4))
# add a ROOT object that supports Draw()
plot.add(ROOT.TLine(1, 250, 1.6, 150))
plot.draw()
plot.saveAs('plots08.png')
```

![doc/figures/plots08.png](doc/figures/plots08.png)

Similar like for `LegendDecorator`, `Plot.defaultTitleDecorator` can be used to change the title attributes of all `Plot` objects created afterwards. If no title should be drawn simply set `plot.drawTitle = False`.

2D histograms
-----
2D histograms can be added to a `Plot` just like 1D histograms. They are always drawn first, i.e. any 1D histogram or graph will be drawn on top.
```python
plot = Plot('My seventh plot', Variable('x'), Variable('y'), Variable('Events'))
plot.drawLegend = False
plot.addHistogram(h2D, 'colz')
plot.draw()
# access to the colour palette, if draw option contained 'z'
plot.palette.SetX1NDC(0.81)
plot.saveAs('plots09.png')
```

![doc/figures/plots09.png](doc/figures/plots09.png)
