from happy.variable import Variable, Binning
from happy.dataMcPlot import DataMcPlot
from happy.histogramTools import histToGraph, addGraphs
# create a Variable to define the x-axis
xVar = Variable( 'Invariant Mass', unit='GeV', binning=Binning( 20, -4, 4 ) )
# create and fill some dummy histograms
h1 = xVar.createHistogram( 'First' )
h1.FillRandom( 'gaus', 1000 )
h1.SetLineColor( 2 )
h2 = xVar.createHistogram( 'Second' )
h2.FillRandom( 'gaus', 500 )
h2.SetLineColor( 4 )
h3 = xVar.createHistogram( 'Data' )
h3.FillRandom( 'gaus', 1500 )
h3.SetLineColor( 1 )
# create some dummy systematics graph (+30%/-20% on h1)
sys = histToGraph( h1, 'hSyst', False )
for i in xrange( sys.GetN() ):
    from ROOT import Double
    x, y = (Double(), Double())
    sys.GetPoint( i, x, y )
    sys.SetPointEYlow( i, 0.2*y )
    sys.SetPointEYhigh( i, 0.3*y )
# add nominal values of h2 to the dummy systematics
addGraphs( sys, histToGraph( h2, '', False ) )
# create and fill a DataMcPlot
dataMcTest = DataMcPlot( 'testDataMcPlot', xVar )
dataMcTest.drawStatError = False
dataMcTest.drawCombinedError = True
dataMcTest.addHistogram( h1 )
dataMcTest.setMCSystematicsGraph( sys )
# add h2 stacked and not stacked. Need to copy one so they can have different style
dataMcTest.addHistogram( h2, stacked=False, copy=True )
dataMcTest.addHistogram( h2 )
dataMcTest.setDataHistogram( h3 )
dataMcTest.draw()
dataMcTest.saveIn()
