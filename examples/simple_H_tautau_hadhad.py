'''
Simplified example of the Htautau hadhad analysis from defining datasets to plots

@author Christian Grefe, Bonn University (christian.grefe@cern.ch)
'''
from happy.style import Style
from happy.dataset import RootDataset,CombinedDataset, HistogramSumOfWeightsCalculator
from happy.dataMcPlot import DataMcPlot
from happy.variable import Variable, Binning
from happy.miscellaneous import interrupt
from happy.cut import Cut
from ROOT import kBlack, kCyan, kYellow
import logging

# Switch on debug output
#logging.root.setLevel( logging.DEBUG )

# IMPORTANT: reading from EOS is supported but not recommended. Download the files and adapt the path accordingly for a huge speed increase!
# IMPORTANT: File discovery expects directory structure 'NTUPPATH/mc15/' and 'NTUPPATH/data/'
basePath = '/Users/Eric/runII/hadhad/ntup/v18/skimIV_nomOnly_BDTtraining/mc15/'
print "Discovering ntuples in:\n%s"%basePath

# First we need to define some datasets to work with. The isData flag is important, for example, this prevents scaling with lumi (data is by definition normalized to its lumi)
data = RootDataset( 'data', 'Data', style=Style( kBlack ), isData=True )
# we can add file names to this dataset individually or use wildcards at directory and file level
dataPath=basePath.replace("mc15","data")
data.fileNames.append( dataPath + '*data15*/*root*' )

# MC and pile-up weights need to be considered for Monte-Carlo samples: they are combined in the weight_total branch
# In addition each sample needs to be normalized by the totalSumOfWeights at the generator. This number is stored in a histogram in the n-tuples
RootDataset.defaultSumOfWeightsCalculator = HistogramSumOfWeightsCalculator( 'h_metadata', 8 )

# The MadGraph Z+Jets Monte-Carlo is sliced into 5 samples with their own cross-sections so we need 5 datasets to represent this
# See https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/CentralMC15ProductionList for cross sections
ztautau_Np0 = RootDataset( '361510', 'Z#rightarrow#tau#tau (Np0)', crossSection=1722.0896, weightExpression='weight_total' )
ztautau_Np0.fileNames.append( basePath + '*.mc15_13TeV.361510.*/*.root*' )
ztautau_Np1 = RootDataset( '361511', 'Z#rightarrow#tau#tau (Np1)', crossSection=260.4448, weightExpression='weight_total' )
ztautau_Np1.fileNames.append( basePath + '*.mc15_13TeV.361511.*/*.root*' )
ztautau_Np2 = RootDataset( '361512', 'Z#rightarrow#tau#tau (Np2)', crossSection=82.760832, weightExpression='weight_total' )
ztautau_Np2.fileNames.append( basePath + '*.mc15_13TeV.361512.*/*.root*' )
ztautau_Np3 = RootDataset( '361513', 'Z#rightarrow#tau#tau (Np3)', crossSection=22.926288, weightExpression='weight_total' )
ztautau_Np3.fileNames.append( basePath + '*.mc15_13TeV.361513.*/*.root*' )
ztautau_Np4 = RootDataset( '361514', 'Z#rightarrow#tau#tau (Np4)', crossSection=8.9626768, weightExpression='weight_total' )
ztautau_Np4.fileNames.append( basePath + '*.mc15_13TeV.361514.*/*.root' )

# Wrap the 5 datasets into a physics process (which can be used as a dataset as well while maintaining the relative cross section scaling)
ztautau = CombinedDataset( 'ztautau', 'Z#rightarrow#tau#tau', style=Style( kCyan+1 ), datasets=[ ztautau_Np0, ztautau_Np1, ztautau_Np2, ztautau_Np3, ztautau_Np4 ] )

# Define some general information
lumi = 3209.05 # in pb-1
sqrts = 13

# Define the cuts that make up the preselection (see https://twiki.cern.ch/twiki/bin/view/AtlasProtected/HiggsToTauTauToHH2016SummerAC)
# single tau cuts
cut_tau_charge = Cut( 'tau_charge', cut='abs(ditau_tau0_q) == 1 && abs(ditau_tau1_q) == 1' )
cut_tau_tracks = Cut( 'tau_tracks', cut='(ditau_tau0_n_tracks == 1 || ditau_tau0_n_tracks == 3) && (ditau_tau1_n_tracks == 1 || ditau_tau1_n_tracks == 3)' )
cut_tau_id = Cut( 'tau_id', cut='n_taus_medium == 2 && n_taus_tight > 0 && ditau_tau0_jet_bdt_medium == 1 && ditau_tau1_jet_bdt_medium == 1' )
cut_tau_isolation = Cut( 'tau_isolation', cut='ditau_tau0_n_wide_tracks == 0 && ditau_tau1_n_wide_tracks == 0' )
cut_tau_pt = Cut( 'tau_pt', cut='ditau_tau0_pt > 40 && ditau_tau1_pt > 30' )
cut_tau_lveto = Cut( 'tau_lveto', cut='selection_lepton_veto ==1 && !ditau_tau0_ele_bdt_loose && !ditau_tau1_ele_bdt_loose' )

# di-tau cuts
cut_ditau_os = Cut( 'ditau_os', cut='selection_opposite_sign == 1' )
cut_ditau_met = Cut( 'ditau_met', cut='selection_met == 1' )
cut_ditau_met_centrality = Cut( 'ditau_met_centrality', cut='selection_met_centrality == 1' )
cut_ditau_delta_eta = Cut( 'ditau_delta_eta', cut='selection_delta_eta == 1' )
cut_ditau_delta_r = Cut( 'ditau_delta_r', cut='selection_delta_r == 1' )

# Define the preselection cut excluding the OS cut
baseCut = cut_tau_charge + cut_tau_tracks + cut_tau_id + cut_tau_isolation + cut_tau_pt + cut_tau_lveto + cut_ditau_met + cut_ditau_met_centrality + cut_ditau_delta_eta + cut_ditau_delta_r
preselection = baseCut + cut_ditau_os
preselection.setNameTitle( 'preselection', 'Preselection' )

# It is a good idea to cache the list of events that pass our base selection to increase speed
for dataset in [ data, ztautau ]:
    dataset.preselection = baseCut

# Define SS control region, note that "-" inverts a cut
cr_ss = baseCut - cut_ditau_os
cr_ss.setNameTitle( 'cr_ss', 'SS CR' )

# poor mans OS-SS fake estimate; start by defining multijet background from data
multijet = CombinedDataset( 'multijet', 'Multijet', style=Style( kYellow+1 ), datasets=[data] )
# whenever we use this, we plot the events from SS instead of OS, assuming charge symmetry in the QCD background
multijet.ignoredCuts.append( cut_ditau_os )
multijet.additionalCuts.append( -cut_ditau_os )

# calculate a simple correction factor assuming there is only ztautau and multijet in the data; shape is still from SS CR
yield_data_cr_ss, yield_data_cr_ss_uncertainty = data.getYield( cr_ss )
yield_data_preselection, yield_data_preselection_uncertainty = data.getYield( preselection )
yield_ztautau_preselection, yield_ztautau_preselection_uncertainty = ztautau.getYield( preselection, luminosity=lumi )
multijet.scaleFactors[ 'rQCD' ] = (yield_data_preselection - yield_ztautau_preselection) / yield_data_cr_ss

print 'Yields'
print '  Data in SS CR: %.3g+-%.3g' % ( yield_data_cr_ss, yield_data_cr_ss_uncertainty )
print '  Data in Preselection: %.3g+-%.3g' % ( yield_data_preselection, yield_data_preselection_uncertainty )
print '  Ztautau in Preselection: %.3g+-%.3g' % ( yield_ztautau_preselection, yield_ztautau_preselection_uncertainty )
print '  rQCD = %.3g' % multijet.scaleFactors[ 'rQCD' ]

# define our set of background samples
backgrounds = [ multijet, ztautau ]

# Define a small helper method to build our data-mc plots
def makePlot( variable, cut ):
    plot = DataMcPlot( cut.name + '_' + variable.name, variable )
    plot.titles.append( 'L = %.2g fb^{-1}, %d TeV' % ( lumi / 1000., sqrts) )
    plot.titles.append( cut.title )
    plot.setDataHistogram( data.getHistogram( variable, cut=cut ) )
    for dataset in backgrounds:
        plot.addHistogram( dataset.getHistogram( variable, cut=cut, luminosity=lumi ) )
    plot.draw()
    plot.saveIn()
    return plot
        
# Define some variable and plot it
v_m_mmc = Variable( 'ditau_mmc_maxw_m', title='M_{MMC}(#tau#tau)', unit='GeV', binning=Binning( 14, 40, 180. ) )
p = makePlot( v_m_mmc, preselection )

# Wait for user to continue
interrupt()
