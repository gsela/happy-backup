## package examples.plots
#  @brief Basic examples for creating plots
#
#  @author Christian Grefe, Bonn University (christian.grefe@cern.ch)
import ROOT
from happy.plot import Plot

# create some ROOT objects to draw
h1 = ROOT.TH1D('h1', 'First histogram', 40, -4, 4)
h1.FillRandom('gaus', 2000)
h1.SetLineColor(ROOT.kRed)
h1.SetMarkerColor(ROOT.kRed)

h2 = ROOT.TH1D('h2', 'Second histogram', 40, -4, 4)
h2.FillRandom('gaus', 1000)
h2.SetMarkerColor(ROOT.kBlue)
h2.SetLineColor(ROOT.kBlue)
h2.SetLineWidth( 3 )
# need to set fill color explicitely to 0 to ensure no fill for stacked histograms
h2.SetFillStyle( 0 )

h3 = ROOT.TH1D('h3', 'Test data', 40, -4, 4)
h3.FillRandom('gaus', 3000)
h3.SetLineColor(ROOT.kBlack)

f1 = ROOT.TF1('f1', 'gaus', -4, 4)
f1.SetLineColor(ROOT.kGreen+2)

f2 = ROOT.TF2('f2','sin(x)*sin(y)/(x*y)+1',-4,4,-4,4);
h2D = ROOT.TH2D('h2D', '2D histogram', 20, -4, 4, 20, -4, 4)
h2D.FillRandom('f2', 50000)

# create a Plot with two histograms
plot = Plot('My first plot')
# adding a histogram with a specific draw option
plot.addHistogram(h1, 'E0')
# default draw option for histograms is 'HIST'
plot.addHistogram(h2)
plot.draw()
plot.saveAs('plots01.png')

# create a Plot with histograms normalised to unity and fitting a function
plot = Plot('My second plot')
# an internal copy can be generated which is useful if the Plot modifies the histogram, e.g. when normalising the histograms
# the function returns the copied object.
h1copy = plot.addHistogram(h1, copy=True)
plot.addHistogram(h2, 'E0', copy=True)
# add a fit function to one of the histograms
plot.addFitFunction(h1copy, f1, legendTitle='Fit to first histogram')
# normalise all histograms to unity
plot.normalized = True
# the fit is performed in the draw step, i.e. after the normalisation step
plot.draw()
plot.saveAs('plots02.png')

# create a Plot with stacked histograms
plot = Plot('My third plot')
plot.addHistogram(h1, stacked=True)
plot.addHistogram(h3, 'E')
plot.addHistogram(h2, stacked=True)
plot.draw()
plot.saveAs('plots03.png')

# define a Variable to set axis labels
from happy.variable import Variable, Binning, binningFromHistogram
varX = Variable('Invariant mass', unit='GeV', binning=Binning(40, -4, 4))
# reading binning from an existing histogram
varX.binning = binningFromHistogram(h1)
# changing the limits
varX.binning.low = -2
varX.binning.high = 3
varY = Variable('Events')
varY.binning.high = 400
plot.variableX = varX
plot.variableY = varY
plot.draw()
plot.saveAs('plots04.png')

# create a histogram with bin labels
var = Variable('Option', binning=Binning(4, -1, 3, ['A','B','C','D']))
# create an empty histogram from a Variable object
h4 = var.createHistogram('Selected option')
h4.FillRandom('gaus', 100)
plot = Plot('My fourth plot')
plot.addHistogram(h4)
plot.draw()
plot.saveAs('plots05.png')

# create a histogram with varying bin widths
from happy.variable import VariableBinning
varX.binning = VariableBinning([-2,-1,-0.5,-0.2,0,0.2,0.5,1,2])
h5 = varX.createHistogram('Variable bin size')
h5.FillRandom('gaus', 1000)
plot = Plot('My fifth plot')
plot.addHistogram(h5)
plot.draw()
plot.saveAs('plots06.png')

# create a Plot with modified legend layout
from happy.plot import LegendElement
from happy.plotDecorator import positions
plot = Plot('My sixth plot', varX)
plot.addHistogram(h1, stacked=True)
plot.addHistogram(h3, 'E')
# add an additional LegendElement
plot.legendElements.append(LegendElement(None, 'Placeholder'))
plot.addHistogram(h2, stacked=True)
# add a label to the legend
plot.legendDecorator.labelText = 'My Label'
plot.legendDecorator.labelTextSize = 0.03
# modify the position and the layout of the legend
plot.legendDecorator.position = positions.LeftTop
plot.legendDecorator.textSize = 0.03
plot.legendDecorator.textFont = 52
plot.legendDecorator.maxEntriesPerColumn = 2
plot.draw()
plot.saveAs('plots07.png')

# create a Plot with title, ATLAS label and additional text
from happy.plotDecorator import AtlasTitleDecorator
from happy.plot import TextElement
# need to remove the old title decorator from this plot as it was drawn before
plot.decorators = []
# define some title lines
plot.titles.append('This is some title text')
plot.titles.append('... and this is another line of title text')
# add the ATLAS label
plot.titleDecorator = AtlasTitleDecorator('Preliminary')
# change the position and the layout of the title
plot.titleDecorator.position = positions.LeftBottom
plot.titleDecorator.textSize = 0.03
# add an additional TextElement
plot.textElements.append(TextElement('Great plot!', alignment=22, color=0, x=0.5, y=0.4))
# add a ROOT object that supports Draw()
plot.add(ROOT.TLine(1, 250, 1.6, 150))
plot.draw()
plot.saveAs('plots08.png')

# create a Plot with a 2D histogram
plot = Plot('My seventh plot', Variable('x'), Variable('y'), Variable('Events'))
plot.drawLegend = False
plot.addHistogram(h2D, 'colz')
plot.draw()
# access to the colour palette, if draw option contained 'z'
plot.palette.SetX1NDC(0.81)
plot.saveAs('plots09.png')
