#!/bin/bash
# cross section list maintained by PMG
echo "Getting PMG XS file..."
svn co svn+ssh://svn.cern.ch/reps/atlasoff/PhysicsAnalysis/AnalysisCommon/PMGTools/trunk/data/ PMG_XS
# cross section list maintained by SUSY group
echo "Getting SUSY XS file..."
[[ -d "SUSY_XS" ]] || svn co svn+ssh://svn.cern.ch/reps/atlasoff/PhysicsAnalysis/SUSYPhys/SUSYTools/trunk/data/ SUSY_XS --depth empty
svn up SUSY_XS/susy_crosssections_13TeV.txt